<div id="sidebar-wrapperss">
    <ul class="sidebar-nav nav navbar-inverse">
        <h5 class="mx-auto">DEVICE LIST</h5>
        <div class="input-group p-1 mt-1">
            <input type="text" class="form-control" placeholder="Search this blog">
            <div class="input-group-append">
            <button class="btn btn-warning" type="button">
                <i class="fa fa-search"></i>
            </button>
            </div>
        </div>
        <div id="html" class="demo mt-2">
            <ul>
                <li data-jstree='{ "opened" : true }'>Lamppost
                    <ul>
                        <li data-jstree='{ "selected" : true }'>Camera 1</li>
                        <li>Camera 2</li>
                    </ul>
                </li>
            </ul>
        </div>
    </ul>
</div>
