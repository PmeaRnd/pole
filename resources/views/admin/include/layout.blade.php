@include('admin.include.header')
@include('admin.include.navbar')
@include('admin.include.sidebar')
@yield('content')
@include('admin.include.footer')
