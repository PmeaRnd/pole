@extends('admin.include.layout')
@section('content')
<section class="content">
<div class="container">
    <h1>All Client Login Credentials</h1>
    <p class="float-right"><a href="{{route('superuser.create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i>&nbsp;create user</a></p>
    <table class="table table-bordered data-table">
        <thead class="bg-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100px" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</section>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script type="text/javascript">

  $(function () {
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('superuser.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click','#delete-btn',function(){
        var id = $(this).attr('data-id');
         bootbox.confirm({
            title: '<i class="fa fa-trash fa-lg"></i>Delete Record?',
            message: "Do you want to delete it or not.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if(result)
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url : "{{url('admin/superuser')}}"+"/"+id,
                        type: "DELETE",
                        data : {},
                        success: function(data, textStatus, jqXHR)
                        {
                            console.info(data);
                            console.log(textStatus);
                            console.log(jqXHR);
                            if(textStatus == 'success'){
                                toastr.success('Success', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                            else{
                                toastr.error('Error', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.info(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            toastr.error('Error', 'Internal error occur.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                        }
                    });
                }
            }
        });
    })

    $(document).on('click','#view-btn',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/superuser')}}"+"/"+id,
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var d = new Date(data.data.created_at);
                    var cur_date = d.toLocaleString();
                    var dialog = bootbox.dialog({
                        title: 'User Information',
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancel'
                            }
                        },
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <div class="form-group row">
                                <label for="inputId" class="col-sm-4 col-form-label">ID</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" id="inputId"  readonly value="`+data.data.id+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-4 col-form-label">NAME</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" id="inputName"  value="`+data.data.name+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext"  value="`+data.data.email+`" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticCreated" class="col-sm-4 col-form-label">CREATED DATE</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext"  value="`+cur_date+`" >
                                </div>
                            </div>
                            `);
                        }, 500);
                    });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    @if(\Session::has('user_edit_success'))

        toastr.success('Success',"{{\Session::get('user_edit_success')}}",{
        showMethod: 'fadeIn',
        showDuration: 300,
        showEasing: 'swing',
        hideMethod: 'fadeOut',
        hideDuration: 1000,
        hideEasing: 'swing',
        extendedTimeOut: 1000,
        positionClass: 'toast-top-center',
        timeOut: 3000,
        escapeHtml: false,
        newestOnTop: true,
        preventDuplicates: false,
        progressBar: true
        });
    @endif
    @if(\Session::has('user_create_success'))
    toastr.success('Success',"{{\Session::get('user_create_success')}}",{
    showMethod: 'fadeIn',
    showDuration: 300,
    showEasing: 'swing',
    hideMethod: 'fadeOut',
    hideDuration: 1000,
    hideEasing: 'swing',
    extendedTimeOut: 1000,
    positionClass: 'toast-top-center',
    timeOut: 3000,
    escapeHtml: false,
    newestOnTop: true,
    preventDuplicates: false,
    progressBar: true
    });
    @endif
  });
</script>
@endsection

