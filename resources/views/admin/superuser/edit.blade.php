@extends('admin.include.layout')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="content">
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-xl-12 mt-5">
            <h3 class="text-center">Edit Super User Credentials</h3>
           <form action="{{url('admin/superuser')}}/{{$user->id}}" method="post">
               @csrf
               @method('PUT')
               <div class="form-group row">
                    <label for="inputId" class="col-sm-4 col-form-label">ID</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control-plaintext" id="inputId"  readonly value="{{$user->id}}">

                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">NAME</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" name="name" value="{{$user->name}}">
                      @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                    <div class="col-sm-8">
                    <input type="text"  class="form-control"  name="email" value="{{$user->email}}" >
                    @if ($errors->has('email')) <p class="text-danger">{{ $errors->first('email') }}</p> @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticCreated" class="col-sm-4 col-form-label">PASSWORD</label>
                    <div class="col-sm-8">
                    <input type="password"  class="form-control" name="password" value="" >
                    @if ($errors->has('password')) <p class="text-danger">{{ $errors->first('password') }}</p> @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticCreated" class="col-sm-4 col-form-label">CONFIRM PASSWORD</label>
                    <div class="col-sm-8">
                    <input type="password"  class="form-control"  name="confirm_password" value="" >
                    @if ($errors->has('confirm_password')) <p class="text-danger">{{ $errors->first('confirm_password') }}</p> @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-success"><i class="fas fa-floppy-o"></i> submit</button>
           </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script>
      $(function(){
        var user = $('#poles').select2({
            placeholder: "Select user to link.",
            tags: true,
        });

      })
  </script>
@endsection

