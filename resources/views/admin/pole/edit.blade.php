@extends('admin.include.layout')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style>
    .master-div{
        border : 1px solid gray;
        padding : 5px;
        border-radius : 5px;
    }


    .setup-card{
        height :100%;
        border :  1px solid gray;
        border-radius : 5px;
        margin : 5px;
        padding: 10px;
    }

    .req{
        color:red;
        font-weight: bold;
    }

    .row-flex {
    display: flex;
    flex-wrap: wrap;
    }

    .field-icon {
    float: right;
    margin-left: -25px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="col-12">
        <h2 class="text-center text-primary p-4">Edit Lamppost and its local component.</h2>
    </div>

<form action="{{url("admin/pole/$pole->identity")}}" method="post">
        @csrf
        @method('PUT')
    <div class="row row-flex">
        <div class="col-6 mb-3">
            <div class="setup-card content">
            <h4 class="text-center text-dark mb-3"><img src="{{asset('public/icon/pole_icon.png')}}" width="30px"></img>&nbsp;Lamppost Setup</h4>
                    <div class="form-group row">
                        <label for="inputName" class="col-4 col-form-label">Name <span class="req">*</span></label>
                        <div class="col-8">
                          <input type="text" class="form-control" id="inputName" placeholder="Enter Pole Name" name="name" value="{{$pole->name}}">
                          @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputIp" class="col-4 col-form-label">IP <span class="req">*</span></label>
                        <div class="col-8">
                          <input type="text" class="form-control" id="inputIp" placeholder="Enter Ip" name="ip" value="{{$pole->ip}}">
                          @if ($errors->has('ip')) <p class="text-danger">{{ $errors->first('ip') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputLocation" class="col-4 col-form-label">Location (option)</label>
                        <div class="col-8">
                          <input type="text" class="form-control" id="inputLocation" placeholder="Enter Pole Location" name="location" value="{{$pole->location}}">
                          @if ($errors->has('location')) <p class="text-danger">{{ $errors->first('location') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputUsers" class="col-4 col-form-label">Link Users (option)</label>
                        <div class="col-8">
                          <select id="users" multiple="multiple" name="users[]" class="form-control">
                            @if($users->count() > 0)
                            @foreach($users as $user)
                               <option value="{{$user->id}}"
                                  @if(count($selectuser) > 0)
                                    @if(in_array($user['id'], $selectuser))
                                      selected
                                    @endif
                                  @endif
                                >{{$user->name}}</option>
                            @endforeach
                          @endif
                          </select>
                          @if ($errors->has('users')) <p class="text-danger">{{ $errors->first('users') }}</p> @endif
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-6 mb-3">
            <div class="setup-card content">
                    <h4 class="text-center text-dark mb-3"><i class="fas fa-camera"></i>&nbsp;PTZ Camera Setup</h4>
                    <div class="form-group row">
                         <label for="inputIp" class="col-sm-4 col-form-label">IP Address <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control" id="inputIp" name="ptz_ip" placeholder="Enter ptz camera ip address" value="{{$pole->camera['ip']}}">
                         @if ($errors->has('ptz_ip')) <p class="text-danger">{{ $errors->first('ptz_ip') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputPtzUsername" class="col-sm-4 col-form-label">Username <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control"  name="ptz_username" id="inputPtzUsername" placeholder="Enter username for ptz camera" value="{{$pole->camera['username']}}">
                         @if ($errors->has('ptz_username')) <p class="text-danger">{{ $errors->first('ptz_username') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputPtzPassword" class="col-sm-4 col-form-label">Password <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="password" class="form-control" id="inputPtzPassword" name="ptz_password" placeholder="Enter password for ptz camera" value="{{$pole->camera['password']}}">
                         <span toggle="#inputPtzPassword" class="fa fa-fw fa-eye field-icon mr-1 toggle-password"></span>
                         @if ($errors->has('ptz_password')) <p class="text-danger">{{ $errors->first('ptz_password') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12">
                            <p class="ml-3 text-info" style="font-weight:bold;">
                               This is configuration setting for identify and control the humanity sensor.
                            </p>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-6">
            <div class="setup-card content">
                    <h4 class="text-center text-dark mb-3"><i class="fas fa-desktop"></i></i>&nbsp;LED Screen Setup</h4>
                    <div class="form-group row">
                         <label for="inputLedIp" class="col-sm-4 col-form-label">IP Address <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control" id="inputLedIp" placeholder="Enter led screen ip address" name="screen_ip" value="{{$pole->screen['ip']}}">
                         @if ($errors->has('screen_ip')) <p class="text-danger">{{ $errors->first('screen_ip') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputLedUsername" class="col-sm-4 col-form-label">Username <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control" id="inputLedUsername" placeholder="Enter username for led screen" name="screen_username" value="{{$pole->screen['username']}}">
                         @if ($errors->has('screen_username')) <p class="text-danger">{{ $errors->first('screen_username') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputLedPassword" class="col-sm-4 col-form-label">Password <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="password" class="form-control" id="inputLedPassword" placeholder="Enter password for led screen" name="screen_password" value="{{$pole->screen['password']}}">
                         <span toggle="#inputLedPassword" class="fa fa-fw fa-eye field-icon mr-1 toggle-password"></span>
                         @if ($errors->has('screen_password')) <p class="text-danger">{{ $errors->first('screen_password') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-12">
                            <p class="ml-3 text-info" style="font-weight:bold;">
                               This is configuration setting for identify and control the LED screen.
                            </p>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-6">
            <div class="setup-card content">
                    <h4 class="text-center text-dark mb-3"><i class="fa fa-fire"></i></i>&nbsp;Sensor Setup</h4>
                    <div class="form-group row">
                         <label for="inputSensorIp" class="col-sm-4 col-form-label">IP <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control" id="inputSensorIp" name="sensor_ip" placeholder="Enter sensor ip address" value="{{$pole->sensor['ip']}}">
                         @if ($errors->has('sensor_ip')) <p class="text-danger">{{ $errors->first('sensor_ip') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSensorUsername" class="col-sm-4 col-form-label">Username <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="text" name="sensor_username" class="form-control" id="inputSensorUsername" placeholder="Enter username for sensor" value="{{$pole->sensor['username']}}">
                         @if ($errors->has('sensor_username')) <p class="text-danger">{{ $errors->first('sensor_username') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSensorPassword" class="col-sm-4 col-form-label">Password <span class="req">*</span></label>
                         <div class="col-sm-8">
                         <input type="password" class="form-control" id="inputSensorPassword"  name="sensor_password" placeholder="Enter password for ptz camera" value="{{$pole->sensor['password']}}">
                         <span toggle="#inputSensorPassword" class="fa fa-fw fa-eye field-icon mr-1 toggle-password"></span>
                         @if ($errors->has('sensor_password')) <p class="text-danger">{{ $errors->first('sensor_password') }}</p> @endif
                         </div>
                     </div>
                     <div class="form-group row">
                         <div class="col-12">
                             <p class="ml-3 text-info" style="font-weight:bold;">
                                This is configuration setting for identify and control the humanity sensor.
                             </p>
                         </div>
                     </div>
            </div>
        </div>
        <div class="col-12 text-center mt-3 mb-5">
            <button type="submit" class="btn btn-lg btn-success"><i class="fas fa-hdd"></i>&nbsp;save configuration</button>
        </div>
      </div>
    </form>
</div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script>
      $(function(){
        var user = $('#users').select2({
            placeholder: "Select user to link.",
            tags: true,
        });

        $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
        input.attr("type", "text");
        } else {
        input.attr("type", "password");
        }
        });

      })
  </script>
@endsection
