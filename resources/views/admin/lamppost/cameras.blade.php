@extends('admin.include.layout')
@section('css')
  <link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
  <link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
  <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
  <script>

    function mouseDownPTZ($data, $email = ""){
        alert('down');
    }

    function mouseUpPTZ($data, $email = ""){
        alert('up');
    }

    function lenplus(){
         alert('lenplus');
     }

     function lenminus(){
         alert('lenminus');
     }

     function focusplus(){
         alert('focusplus');
     }

     function focusminus(){
         alert('focusminus');
     }

     function apertureplus(){
         alert('apertureplus');
     }

     function apertureminus(){
         alert('apertureminus');
     }

     $(function(){
          $('#single').click(function(){
              $(this).addClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').show();
              $('#two').hide();
              $('#three').hide();
              $('#four').hide();
              $('#five').hide();
          });

          $('#double').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').show();
              $('#three').hide();
              $('#four').hide();
              $('#five').hide();
          });

          $('#tripple').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').show();
              $('#four').hide();
              $('#five').hide();
          });

          $('#fourth').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').hide();
              $('#four').show();
              $('#five').hide();
          });

          $('#fifth').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').hide();
              $('#four').hide();
              $('#five').show();
          });
     });
  </script>
@endsection
@section('content')
  <div class="container-fluid m-0 p-0" style="background-color:white">
      <div class="row">
          <div class="col-md-9 col-sm-9 col-xl-9 m-0 p-0">
             <div class="row m-1" id="one" style="">
                 <div class="col-12" style="height:640px">
                    {{-- Select camera to view --}}
                 </div>
             </div>
            <div class="row m-0 p-0" id="two"  style="display:none">
                <div class="col-6 " >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6 " >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6 " >
                    <div class=" m-1 " style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6" >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6" >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
            </div>
            <div class="row m-1" id="three"  style="display:none">
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
            </div>
            <div class="row m-1" id="four"  style="display:none">
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
            </div>
          </div>
          <div class="col-md-3 m-0 p-0">
            @include('admin.include.rsidebar')
          </div>
      </div>
  </div>


@endsection
@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
<script src="{{asset('public/js/pictarea.js')}}"></script>
  {{-- <script src="{{asset('public/js/stream/webVideoCtrl.js')}}"></script> --}}
  {{-- <script src="{{asset('public/js/stream/hikapi.js')}}"></script> --}}
  {{-- <script src="{{asset('public/js/stream/monitor.js')}}"></script> --}}
  {{-- <script src="https://open.ys7.com/sdk/js/2.0/ezuikit.js"></script> --}}
<script>
$(function() {
     function log(str){
         var div = document.createElement('DIV');
         div.innerHTML = (new Date()).Format('yyyy-MM-dd hh:mm:ss.S') + JSON.stringify(str);
         document.body.appendChild(div);
     }

      var $tabButtonItem = $('#tab-button li'),
      $tabSelect = $('#tab-select'),
      $tabContents = $('.tab-contents'),
      activeClass = 'is-active';

      $tabButtonItem.first().addClass(activeClass);
      $tabContents.not(':first').hide();

      $tabButtonItem.find('a').on('click', function(e) {
          var target = $(this).attr('href');
          $tabButtonItem.removeClass(activeClass);
          $(this).parent().addClass(activeClass);
          $tabSelect.val(target);
          $tabContents.hide();
          $(target).show();
          e.preventDefault();
      });

      $tabSelect.on('change', function() {
          var target = $(this).val(),
          targetSelectNum = $(this).prop('selectedIndex');
          $tabButtonItem.removeClass(activeClass);
          $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
          $tabContents.hide();
          $(target).show();
      });
    });

  $(function(){
      function cameralist(keyword = null){
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          $.ajax({
            url : "{{route('camera.list')}}",
            type : 'POST',
            data : {keyword : keyword},
            success : function(data){
                // console.log(data.data);
                $('.load-cameras').html('');
               var str = `<div id="html" class="demo mt-2">`;
               if(data.data.length > 0){
                   for(i = 0; i < data.data.length; i++){
                       str += `<ul class="add_data">`;
                       var active = '';
                       if(i === 0){
                           active = `data-jstree='{"opened" : true}'`;
                       }
                       str += `<li `+active+`>`+data.data[i].name;
                       str += `<ul>
                                    <li `+active+`>`+data.data[i].camera_ip+`</li>
                                    <li >test</li>
                                </ul></li>`;
                       str += "</ul>";

                   }
                   if(i === data.data.length - 1){
                       str += "</div>";
                   }
                   $('.load-cameras').append(str);
                   $('#html').on('changed.jstree', function (e, data) {
                           var i, j, r = [];
                           for(i = 0, j = data.selected.length; i < j; i++) {
                           r.push(data.instance.get_node(data.selected[i]).text);
                           }
                           console.log(r);
                           // $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                           // $.ajax({
                           //     url : "{{route('camera.stream')}}",
                           //     type : 'POST',
                           //     data : {camera_ips : r},
                           //     success : function(result){
                           //         console.log(result);
                           //     },
                           //     error : function(error){
                           //         console.log(error);
                           //     }
                           // });
                       }).jstree({'plugins': ['search', 'checkbox', 'wholerow']});
               }
               else{
                   var nodata = "<div class='mt-2 text-center'><span class='badge badge-warning'>No result found.</span></div>";
                   $('.load-cameras').html(nodata);
               }
            },
            error : function(error){
               console.log('camera list load error.');
               console.log(error);
            }
          });
      }

      // Default load the All lamppost its related camera list.
      cameralist();

      // App the jstree after loaded all lamppost
      setTimeout(function(){
        // $('#html').on('changed.jstree', function (e, data) {
        //         var i, j, r = [];
        //         for(i = 0, j = data.selected.length; i < j; i++) {
        //         r.push(data.instance.get_node(data.selected[i]).text);
        //         }
        //         $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        //         $.ajax({
        //             url : "{{route('camera.stream')}}",
        //             type : 'POST',
        //             data : {camera_ips : r},
        //             success : function(result){
        //                 console.log(result);
        //             },
        //             error : function(error){
        //                 console.log(error);
        //             }
        //         });
        //     }).jstree({'plugins': ['search', 'checkbox', 'wholerow']});
        },3000);

      $('#search').keyup(function(){
          var keyword = $(this).val();
          cameralist(keyword);
          setTimeout(function(){
            $('#html').jstree();
            },1000);
      })
  })
  </script>
@endsection
