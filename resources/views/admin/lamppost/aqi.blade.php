@extends('admin.include.layout')
@section('css')
  <link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
  <link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
@endsection
@section('content')

  <div class="container-fluid" style="background-color:white;height:640px;width:100%">
      <div class="row h-100">
          <div class="col-md-9 col-sm-9 col-xl-9">

          </div>
          <div class="col-md-3 bg-dark ">
            @include('admin.include.aqisidebar')
          </div>
      </div>
  </div>
@endsection
@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
  <script>
$(function() {
  var $tabButtonItem = $('#tab-button li'),
      $tabSelect = $('#tab-select'),
      $tabContents = $('.tab-contents'),
      activeClass = 'is-active';

  $tabButtonItem.first().addClass(activeClass);
  $tabContents.not(':first').hide();

  $tabButtonItem.find('a').on('click', function(e) {
    var target = $(this).attr('href');

    $tabButtonItem.removeClass(activeClass);
    $(this).parent().addClass(activeClass);
    $tabSelect.val(target);
    $tabContents.hide();
    $(target).show();
    e.preventDefault();
  });

  $tabSelect.on('change', function() {
    var target = $(this).val(),
        targetSelectNum = $(this).prop('selectedIndex');

    $tabButtonItem.removeClass(activeClass);
    $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
    $tabContents.hide();
    $(target).show();
  });
});

$('#html').jstree();
  </script>
@endsection
