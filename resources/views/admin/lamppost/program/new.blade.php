@extends('admin.include.layout')
@section('css')
@endsection
@section('content')
  <div class="container-fluid mt-2">
    <div class="card mr-0">
        <div class="card-header bg-light m-1">
           <div class="row float-right">
            <button class="btn btn-primary"><i class="fa fa-cloud"></i> save </button>&nbsp;
            <button class="btn btn-dark"><i class="fa fa-eye"></i> preview </button>&nbsp;
            <button class="btn btn-danger"><i class="fa fa-trash"></i> delete </button>&nbsp;
            <button class="btn btn-secondary"><i class="fa fa-th-list"></i> list </button>
           </div>
        </div>
        <div class="card-body pt-0 pb-0 mr-0" >
          <div class="row" >
              <div class="col-md-3 border-right " style="height:100%">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-3 col-form-label">Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputWidth" class="col-sm-6 col-form-label">Display width</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="inputWidth" placeholder="width">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputHeight" class="col-sm-6 col-form-label">Height</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="inputHeight" placeholder="height">
                    </div>
                </div>
                <div class="row mr-2 "  >
                    <button class="btn btn-block btn-dark"><i class="fa fa-text-height"></i>&nbsp;  Add Text Program</button>
                    <button class="btn btn-block btn-dark"><i class="fa fa-camera"></i>&nbsp;  Add Photo Program</button>
                    <button class="btn btn-block btn-dark"><i class="fa fa-table"></i>&nbsp;  Add Table Program</button>
                    <button class="btn btn-block btn-dark"><i class="fa fa-play"></i>&nbsp;  Add Video Program</button>
                    <button class="btn btn-block btn-dark"><i class="fa fa-headphones"></i>&nbsp;  Add Background Audio</button>
                    <button class="btn btn-block btn-dark"><i class="fa fa-wrench"></i>  Setup Real Time Info</button>
                </div><br><br><br><br><br><br><br><br><br><br>
              </div>
              <div class="col-md-7 border-right ">
                 <div class="row">
                     <div class="col-md-2  " style="width:100%;">
                        <div   class="bg-dark" style="width:100%;height:100%">

                        </div>
                     </div>
                     <div class="col-md-2 bg-dark " style="height:100px;width:100%;">

                    </div>
                 </div>
              </div>
              <div class="col-md-2 m-0 p-0" style="height:100%">
                <div style="height:40vh">
                     sdsddfsad
                </div>
                <div  style="height:30vh; line-height:2" class="border-top">
                   <h4 class="bg-dark m-0 p-0" style="font-weight:bold"> Property</h5>
                   <label class="ml-1">Pause Time(s)</label>
                   <input type="text" name="" id="" class="form-control ml-2">
                </div>
              </div>
          </div>
        </div>
      </div>
  </div>
@endsection
@section('script')
@endsection
