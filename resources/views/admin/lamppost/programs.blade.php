@extends('admin.include.layout')
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
    td{
        text-align:center;
    }
</style>
@endsection
@section('content')
<section class="content">
<div class="container-fluid"><br>
    <p class="mt-1">
        <div class="row">
        <div class="col-md-8 mb-2">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date"  />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date"  readonly/>
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
            </div>
        </div>
        <div class="col-md-4 float-right mb-2">
            <a href="{{route('user.create')}}" class="btn btn-danger "> <i class="fa fa-trash"></i>&nbsp;Delete Program</a>
            <button class="btn btn-primary " id="new-btn"> <i class="fa fa-plus"></i>&nbsp;New Program</button>
        </div>
    </div>
    </p>
    <table class="table table-sm table-bordered data-table table-hover ">
        <thead class="bg-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Height</th>
                <th>Width</th>
                <th>Tag</th>
                <th>Creator</th>
                <th>Renew Time</th>
                <th width="150px" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</section>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script type="text/javascript">



  $(function () {
    var options={
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd hh:mm:ss',
        todayHighlight: true,
        autoclose: true,
      };
    $('#from_date').datetimepicker(options);

    $('#from_date').change(function(){
        var from = $(this).val();
        if(from !== ''){
            $('#to_date').datetimepicker(options);
           $('#to_date').removeAttr('readonly');
        }
        else{
            $('#to_date').attr('readonly',true);
        }
    })
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('programs.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'height', name: 'height'},
            {data: 'width', name: 'width'},
            {data: 'tag', name: 'tag'},
            {data: 'creator', name: 'creator'},
            {data: 'renew_times', name: 'renew_times'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click','#delete-btn',function(){
        var id = $(this).attr('data-id');
         bootbox.confirm({
            title: '<i class="fa fa-trash fa-lg"></i>Delete Record?',
            message: "Do you want to delete it or not.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if(result)
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url : "{{url('admin/lamppost/programs')}}"+"/"+id,
                        type: "DELETE",
                        data : {},
                        success: function(data, textStatus, jqXHR)
                        {
                            console.info(data);
                            console.log(textStatus);
                            console.log(jqXHR);
                            if(textStatus == 'success'){
                                toastr.success('Success', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                            else{
                                toastr.error('Error', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.info(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            toastr.error('Error', 'Internal error occur.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                        }
                    });
                }
            }
        });
    })

    $(document).on('click','#view-btn',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/programs')}}"+"/"+id,
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var url = "{{asset('public/media')}}";
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">Preview Program</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <video width="100%" height="100%" controls>
                                <source src="`+url+`/`+data.media_link+`" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>`);
                        }, 500);
                    });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    $(document).on('click','#new-btn',function(){
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/programs/info')}}",
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">New Program</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <form action="`+"{{url('admin/lamppost/programs/make')}}"+`" method="post">
                            @csrf
                            <div class="radio">
                               <label>
                                  <input type="radio" name="optradio" checked>Manual Input
                               </label>
                            </div>
                            <div class="row">
                               <div class="col-md-6">
                                 Height : <input type="number" class="form-control" name="height">
                               </div>
                               <div class="col-md-6">
                                 Width : <input type="number" class="form-control" name="width">
                               </div>
                            </div><br>
                            <div class="radio">
                               <label>
                                  <input type="radio" name="optradio" id="display">Select LED Display
                               </label>
                            </div>
                            <div class="row">
                               <select name="display" class="form-control">
                                 <option value="">160 x 120</option>
                                 <option value="">260 x 220</option>
                               </select>
                            </div><br>
                            <button type="submit" class="btn btn-success">New</button></form>
                            `);
                        }, 500);
                    });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    $(document).on('click','#send-btn',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/programs/send')}}"+"/"+id,
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">Display Availables</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <table class="table table-sm table-bordered table-striped">
                              <thead>
                                <tr class="text-center">
                                  <th>Name</th>
                                  <th>Width</th>
                                  <th>Height</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="text-center">
                                  <td>Jone Doe</td>
                                  <td>280</td>
                                  <td>150</td>
                                  <td class="text-success">Online</td>
                                </tr>
                              </tbody>
                            </table>
                            `);
                        }, 500);
                    });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    @if(\Session::has('user_edit_success'))

        toastr.success('Success',"{{\Session::get('user_edit_success')}}",{
        showMethod: 'fadeIn',
        showDuration: 300,
        showEasing: 'swing',
        hideMethod: 'fadeOut',
        hideDuration: 1000,
        hideEasing: 'swing',
        extendedTimeOut: 1000,
        positionClass: 'toast-top-center',
        timeOut: 3000,
        escapeHtml: false,
        newestOnTop: true,
        preventDuplicates: false,
        progressBar: true
        });
    @endif
    @if(\Session::has('user_create_success'))
    toastr.success('Success',"{{\Session::get('user_create_success')}}",{
    showMethod: 'fadeIn',
    showDuration: 300,
    showEasing: 'swing',
    hideMethod: 'fadeOut',
    hideDuration: 1000,
    hideEasing: 'swing',
    extendedTimeOut: 1000,
    positionClass: 'toast-top-center',
    timeOut: 3000,
    escapeHtml: false,
    newestOnTop: true,
    preventDuplicates: false,
    progressBar: true
    });
    @endif
  });
</script>
@endsection


