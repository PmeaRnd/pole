@extends('admin.include.layout')

@section('css')
<link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
<link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
<link rel="stylesheet" href="{{asset('public/css/display.css')}}">
<link rel="stylesheet" href="{{asset('public/picker/dist/bcp.min.css')}}">
<style type="text/css">
    /* .jstree li > a > .jstree-icon {  "background-image":url("{{ asset('public/tree/lamp.png') }}") !important; } */


</style>
@endsection

@section('content')
   <div class="container-fluid m-0 bg-light tb-0">
       <div class="row">
        <div class="col-2 border-right m-0 p-0 border-bottom" style="height:700px;">
        {{-- tree view --}}
        <div id="html" class="demo mt-2">
            <ul>
            <li data-jstree='{ "opened" : true,"icon":"{{asset('public/tree/lamp.png')}}"}'>Lamppost
                    <ul>
                        <li data-jstree='{ "selected" : true }'>Camera 1</li>
                        <li>Camera 2</li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        <div class="col-10 m-0 p-0 border-bottom" >
             {{-- write here --}}
             <div class="row">
                <div class=" col-sm-12 col-xs-12 col-md-12 col-lg-6 col-xl-6">
                  <div class="row" >
                    <div class="mx-auto mt-5" style="height:430px;width:300px;align-self-center;border:1px solid black;" id="box-con">
                    {{-- <img src="{{asset('public/images/core/led.jpg')}}" width="300px" height="430px"  id="display" style="margin-top:auto;margin-bottom:auto;" class="vertical-center"> --}}
                    </div>
                  </div>
                  <h5 class="text-center mt-2 text-warning mt-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LED DISPLAY</h5>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 col-xl-6">
                   <div class="card p-3" style="height:100%">
                          <div class="row">
                              <div class="col-4">
                                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical"><a class="nav-link active" data-toggle="pill" href="#v-texts" role="tab">Text</a> <a class="nav-link" data-toggle="pill" href="#v-images" role="tab">Image</a> <a class="nav-link" data-toggle="pill" href="#v-videos" role="tab">Video</a> </div>
                              </div>
                              <div class="col-8">
                                  <div class="tab-content" id="v-pills-tabContent">
                                      <div class="tab-pane fade show active" id="v-texts" role="tabpanel" aria-labelledby="v-texts-tab">
                                        <form action="#" method="post" enctype="multipart/form-data" id="text-form">
                                          <!-- Material input -->
                                          <p class="text-center">SET TEXT ON DISPLAY</p>
                                          <input type="hidden" name="fileType" value="Text">
                                          <input type="text" id="input-text" class="form-control bufferText" placeholder="Input Text" name="uploadText" >
                                          <div class="row">
                                            <div class="col-12 mt-2">
                                               <input type="hidden" name="align" id="align" value="">
                                              <div class="form-group row">
                                                  <label for="staticText" class="col-sm-4 col-form-label font-weight-bold" style="font-size:18px;">&nbsp;ALIGN :</label>
                                                  <div class="col-sm-8 mt-2">
                                                    <i class="fa fa-2x fa-align-left text-dark left" id="left"></i>&nbsp;&nbsp;
                                                    <i class="fa fa-2x fa-align-center text-dark center" id="center"></i>&nbsp;&nbsp;
                                                    <i class="fa fa-2x fa-align-right text-dark right" id="right" ></i>
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                <label for="staticText" class="col-sm-4 col-form-label font-weight-bold" style="font-size:18px;">&nbsp;Weight :</label>
                                                <input type="hidden" name="bold" id="bold" value="">
                                                <input type="hidden" name="italic" id="italic" value="">
                                                <div class="col-sm-8 mt-2">
                                                  <i class="fa fa-lg fa-bold" id="fa-bold"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <i class="fa fa-lg fa-italic" id="fa-italic"></i>
                                                </div>
                                               </div>
                                               <input type="hidden" name="size" value="" id="size">
                                               <div class="form-group row">
                                                <label for="staticText" class="col-sm-4 col-form-label font-weight-bold" style="font-size:18px;">&nbsp;Size :</label>
                                                <div class="col-sm-4 mt-2">
                                                  <input type="number" min="1" name="fontSize" id="fontSize" class="form-control">
                                                </div>
                                                <div class="col-sm-4">

                                                </div>
                                               </div>
                                               <input type="hidden" name="color" id="color" value="">
                                               <div class="form-group row">
                                                <label for="staticText" class="col-sm-4 col-form-label font-weight-bold" style="font-size:18px;">&nbsp;Color :</label>
                                                <div class="col-sm-4 mt-2">
                                                    <button class="colorpicker btn btn-primary">Pick Color</button>
                                                </div>
                                                <div class="col-sm-4">

                                                </div>
                                               </div><br><br><br><br>
                                               <div class="text-center">
                                                 <button class="btn btn-dark">Send&nbsp;&nbsp;<i class="fa fa-paper-plane"></i></button>
                                               </div>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="tab-pane fade" id="v-images" role="tabpanel" aria-labelledby="v-images-tab">
                                          <p class="text-center">SET IMAGE ON DISPLAY</p>
                                          <form action="#" method="post" enctype="multipart/form-data" id="image-form">
                                          <input type="hidden" name="fileType" value="Image">
                                          <div class="custom-file">
                                              <input type="file" class="custom-file-input" id="imgInp" name="imgUpload">
                                              <label class="custom-file-label" for="imgInp">Choose image file</label>
                                          </div>
                                          <br><br><br><br>
                                            <div class="text-center">
                                                <button class="btn btn-dark">Send&nbsp;&nbsp;<i class="fa fa-paper-plane"></i></button>
                                            </div>
                                          </form>
                                      </div>
                                      <div class="tab-pane fade" id="v-videos" role="tabpanel" aria-labelledby="v-videos-tab">
                                         <p class="text-center">SET VIDEO ON DISPLAY</p>
                                         <form action="#" method="post" enctype="multipart/form-data" id="video-form" class="toDisplay">
                                            <input type="hidden" name="fileType" value="Video">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input video-input" id="videoInp" name="videoUpload">
                                                <label class="custom-file-label" for="videoInp">Choose video file</label>
                                            </div><br><br><br><br>
                                            <div class="text-center">
                                                <button class="btn btn-dark">Send&nbsp;&nbsp;<i class="fa fa-paper-plane"></i></button>
                                            </div>
                                         </form>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                </div>
             </div>
        </div>
       </div>
   </div>
@endsection

@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
<script src="{{asset('public/picker/dist/bcp.min.js')}}"></script>
<script src="{{asset('public/picker/dist/bcp.en.min.js')}}"></script>

<script>
        $('#html').jstree();

        $("#html i.jstree-icon").click(function(){
            $(this).css("backgroundColor", "white");
        });

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#box-con').html('');
                $('#box-con').append(`
                <img src="`+e.target.result+`" width="300px" height="430px"  id="display" style="margin-top:auto;margin-bottom:auto;" class="vertical-center">
                `);
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
          readURL(this);
        });

        $("#videoInp").change(function(e){
          e.preventDefault();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          $.ajax({
                url : "{{route('buffer')}}",
                type : 'POST',
                data:  new FormData(document.getElementById("video-form")),
                contentType: false,
                cache: false,
                processData:false,
                success : function(data){
                    console.log(data);
                    if(data.success){
                       $('#box-con').html('');
                       $('#box-con').append(`
                        <video controls autoplay class="videoInsert" style="width:300px;height:430px;">
                            <source src="`+data.data.url+`" id="uploaded-video" style="width:300px;height:430px;">

                         </video>
                       `);
                       $('#uploaded-video').attr('src',data.data.url);
                    }
                    else{
                    }
                },
                error : function(err){
                    console.log(err);
                }
            });
        });

        $(".bufferText").keyup(function(){
            $('#box-con').html('');
                var text = $(this).val();

                var html = ` <div  style="row justify-content-center; word-wrap: break-word;width:300px;height:430px;" id="ptext">
                             <p id="textLine">`+text+`</p>
                            </div>`;
                $('#box-con').append(html);
        });

        $(".left, .center, .right, .fa-bold, .fa-italic").hover(function() {

            $(this).toggleClass("togclass");

        });

        $(".left, .center, .right, .fa-italic, .fa-bold").click(function() {
                var idName = $(this).attr('id');
                if(idName == 'left'){
                    $('#ptext').removeClass('text-center');
                    $('#ptext').removeClass('text-right');
                    $('#ptext').addClass('text-left');
                    $('#align').val('Left');
                }
                else if(idName == 'center'){
                    $('#ptext').removeClass('text-left');
                    $('#ptext').removeClass('text-right');
                    $('#ptext').addClass('text-center');
                    $('#align').val('Center');
                }
                else if(idName == 'right'){
                    $('#ptext').removeClass('text-left');
                    $('#ptext').removeClass('text-center');
                    $('#ptext').addClass('text-right');
                    $('#align').val('Right');
                }
                else if(idName == 'fa-bold'){
                    $('#ptext').css("font-weight","Bold");
                    $('#bold').val('Bold');
                }
                else if(idName == 'fa-italic'){
                    $('#ptext').css('font-style','italic');
                    $('#italic').val('Italic');
                }

                var myclass = $(this).attr('class');
                if(myclass == 'left' || myclass == 'right' || myclass == 'center'){
                    $('#align').val(myclass);
                }
                else if(myclass == 'fa-bold' || myclass == 'fa-italic'){
                    var wClass = myclass.substring(3, 10);
                    $(`#wClass`).val(myclass.substring(3, 10));
                }
                $(this).addClass('.togclass');
        });

        $('.colorpicker').bcp();
        $('.colorpicker').click(function(e){
           e.preventDefault();
        });
        $('.colorpicker').on('pcb.refresh', function (e) {
            e.preventDefault();
            let color = $(this).bcp('color');
            if (color.value) {
                $(this).css({
                    backgroundColor: color.value,
                    borderColor: color.value,
                    color: color.dark ? '#fff' : '#000'
                });
                $('#textLine').css('color',color.value);
                $('#color').val(color.value);
            }
            else{
                $('#color').val('#ffffff');
            }
        });

        $('#fontSize').keyup(function(){
           var size = $(this).val();
           $('#ptext').css('font-size',size+'px');
           $('#size').val(size);
        });

        $('#video-form, #image-form, #text-form').submit(function(e){
          e.preventDefault();
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          $.ajax({
                url : "{{route('display.upload')}}",
                type : 'POST',
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success : function(data){
                    if(data.success){
                       console.log(data);
                    }
                    else{
                        console.log(data);
                    }
                },
                error : function(err){
                    console.log(err);
                }
            });
        })
</script>
@endsection
