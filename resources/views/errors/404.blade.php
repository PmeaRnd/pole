<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{asset('public/error/404/style.css')}}" />
    <title>404 Page not found</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                 <div id="notfound">
                     <div class="notfound">
                         <div class="notfound-404">
                             <h1>4<span></span>4</h1>
                         </div>
                         <h2>Oops! Page Not Be Found</h2>
                         <p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
                         <a href="{{url('/')}}">Back to homepage</a>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</body>
</html>

