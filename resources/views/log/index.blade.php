@extends('admin.include.layout')
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="content">
<div class="container-fluid"><br>
    <p class="mt-1">
        <div class="row">
        <div class="col-md-6 offset-md-3 mb-2">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date"  />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date"  readonly/>
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
            </div>
        </div>

    </div>
    </p>
    <table class="table table-sm table-bordered data-table table-hover ">
        <thead class="bg-dark">
            <tr class="text-center">
                <th>#</th>
                <th>User</th>
                <th>Subject</th>
                <th>Method</th>
                <th width="70px" class="text-center">IP</th>
                <th>Agent</th>
                <th width="100px" class="text-center">Created At</th>
                <th width="100px" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</section>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script type="text/javascript">



  $(function () {
    var options={
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd hh:mm:ss',
        todayHighlight: true,
        autoclose: true,
      };
    $('#from_date').datetimepicker(options);

    $('#from_date').change(function(){
        var from = $(this).val();
        if(from !== ''){
            $('#to_date').datetimepicker(options);
           $('#to_date').removeAttr('readonly');
        }
        else{
            $('#to_date').attr('readonly',true);
        }
    })
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('logs.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
            {data: 'user_name', name: 'user_name'},
            {data: 'subjects', name: 'subjects'},
            {data: 'url_method', name: 'url_method'},
            {data: 'ip', name: 'ip'},
            {data: 'agents', name: 'agents'},
            {data: 'new_time', name: 'new_time'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click','#delete-btn',function(){
        var $button = $(this);
        var id = $(this).attr('data-id');

         bootbox.confirm({
            title: '<i class="fa fa-trash fa-lg"></i>Delete Record?',
            message: "Do you want to delete it or not.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if(result)
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url : "{{url('admin/lamppost/logs')}}"+"/"+id,
                        type: "DELETE",
                        data : {},
                        success: function(data, textStatus, jqXHR)
                        {
                            console.info(data);
                            console.log(textStatus);
                            console.log(jqXHR);
                            if(textStatus == 'success'){
                                table.row( $button.parents('tr') ).remove().draw();
                                toastr.success('Success', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                            else{
                                toastr.error('Error', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.info(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            toastr.error('Error', 'Internal error occur.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                        }
                    });
                }
            }
        });
    })

    $(document).on('click','#view-btn',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/logs')}}"+"/"+id,
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                console.log(data);
                if(textStatus == 'success'){
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">Log Detail</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <div class="form-group row">
                                <label for="staticName" class="col-sm-4 col-form-label">User Name</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticName" value="`+data.data.user_name+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticSubject" class="col-sm-4 col-form-label">Subject</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticSubject" value="`+data.data.subject+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticUrl" class="col-sm-4 col-form-label">Url</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticUrl" value="`+data.data.url+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticMethod" class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticMethod" value="`+data.data.method+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticIp" class="col-sm-4 col-form-label">IP</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticIp" value="`+data.data.ip+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticAgent" class="col-sm-4 col-form-label">Agent</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticAgent" value="`+data.data.agent+`">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticCreate" class="col-sm-4 col-form-label">Created At</label>
                                <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="staticCreate" value="`+data.data.created_at+`">
                                </div>
                            </div>`);
                        }, 500);
                    });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

  });
</script>
@endsection


