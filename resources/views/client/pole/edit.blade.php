@extends('admin.include.layout')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="container-fluid">
        <div class="pole_form p-5">
        <form action="{{url("admin/pole/$pole->identity")}}" method="post">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-12 mb-3">
                    <h2 class="text-center text-primary">Edit Pole</h2>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-4 col-form-label">Name</label>
                <div class="col-8">
                <input type="text" class="form-control" id="inputName" placeholder="Enter Pole Name" name="name" value="{{$pole->name}}">
                  @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="inputIp" class="col-4 col-form-label">IP</label>
                <div class="col-8">
                <input type="text" class="form-control" id="inputIp" placeholder="Enter Ip" name="ip" value="{{$pole->ip}}">
                  @if ($errors->has('ip')) <p class="text-danger">{{ $errors->first('ip') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="inputLocation" class="col-4 col-form-label">Location</label>
                <div class="col-8">
                <input type="text" class="form-control" id="inputLocation" placeholder="Enter Pole Location" name="location" value="{{$pole->location}}">
                  @if ($errors->has('location')) <p class="text-danger">{{ $errors->first('location') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="inputUsers" class="col-4 col-form-label">Link Users</label>
                <div class="col-8">
                  <select id="poles" multiple="multiple" name="users[]" class="form-control">
                      @if($users->count() > 0)
                        @foreach($users as $user)
                           <option value="{{$user->id}}"
                              @if(count($selectuser) > 0)
                                @if(in_array($user['id'], $selectuser))
                                  selected
                                @endif
                              @endif
                            >{{$user->name}}</option>
                        @endforeach
                      @endif
                  </select>
                  @if ($errors->has('users')) <p class="text-danger">{{ $errors->first('users') }}</p> @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-success">submit</button>
                </div>
            </div>
        </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script>
      $(function(){
        var user = $('#users').select2({
            placeholder: "Select user to link.",
            tags: true,
        });

      })
  </script>
@endsection
