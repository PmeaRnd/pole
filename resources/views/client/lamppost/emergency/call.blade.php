@extends('admin.include.layout')
@section('css')

@endsection
@section('content')
   <div class="container-fluid h-100">
       <div class="row justify-content-center h-100" >
           <div class="col-8 border-right" style="height:700px;">
               <div style="bottom:0px;left:0px;width:100%" class="border rounded border-default mt-2 position-absolute">
                  <div class="row text-primary m-1">
                     <div class="col-md-4">
                        <h5 class="m-2">Name : 4433</h5>
                     </div>
                     <div class="col-md-4">
                        <h5 class="mt-2 text-center m-2">Time : 2020-02-08 19:31:22</h5>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-warning float-right m-2"><i class="fa fa-plug"></i> HANG UP</button>
                    </div>
                  </div>
                  <hr class="m-0">
                  <div class="row text-primary m-1">
                    <div class="col-md-2">
                       <h5 class="m-2">Remarks</h5>
                    </div>
                    <div class="col-md-7">
                       <input type="text" name="" id="" class="form-control m-2">
                   </div>
                   <div class="col-md-3">
                       <button class="btn btn-primary float-right m-2">SUBMIT A REMARK</button>
                   </div>
                 </div>
               </div>
           </div>
           <div class="col-4">
              <div class="row mt-3 ml-1 mr-1 mb-1">
                  <div class="col-2">
                    <i class="fa fa-play-circle fa-3x"></i>
                  </div>
                  <div class="col-6">
                     <h5>44573</h5>
                     <p style="font-size:14px;">2020-02-08 19:47:51</p>
                  </div>
                  <div class="col-3">
                     <button class="btn btn-warning mt-1 float-right">PENDING</button>
                  </div>
              </div>
              <div class="row mt-3 ml-1 mr-1 mb-1">
                <div class="col-2">
                  <i class="fa fa-play-circle fa-3x"></i>
                </div>
                <div class="col-6">
                   <h5>44573</h5>
                   <p style="font-size:14px;">2020-02-08 19:47:51</p>
                </div>
                <div class="col-3">
                   <button class="btn btn-success mt-1 float-right">PROCESSING</button>
                </div>
            </div>
           </div>
       </div>
   </div>
@endsection
@section('script')
@endsection
