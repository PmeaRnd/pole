@extends('client.include.layout')
@section('css')
  <link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
  <link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
  {{-- <script src="{{asset('public/js/stream/hikapi.js')}}"></script> --}}
  <script>

    function mouseDownPTZ($data, $email = ""){
        alert('down');
    }

    function mouseUpPTZ($data, $email = ""){
        alert('up');
    }

    function lenplus(){
         alert('lenplus');
     }

     function lenminus(){
         alert('lenminus');
     }

     function focusplus(){
         alert('focusplus');
     }

     function focusminus(){
         alert('focusminus');
     }

     function apertureplus(){
         alert('apertureplus');
     }

     function apertureminus(){
         alert('apertureminus');
     }

     $(function(){
          $('#single').click(function(){
              $(this).addClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').show();
              $('#two').hide();
              $('#three').hide();
              $('#four').hide();
              $('#five').hide();
          });

          $('#double').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').show();
              $('#three').hide();
              $('#four').hide();
              $('#five').hide();
          });

          $('#tripple').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#fourth').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').show();
              $('#four').hide();
              $('#five').hide();
          });

          $('#fourth').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fifth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').hide();
              $('#four').show();
              $('#five').hide();
          });

          $('#fifth').click(function(){
              $(this).addClass('active');
              $('#single').removeClass('active');
              $('#double').removeClass('active');
              $('#tripple').removeClass('active');
              $('#fourth').removeClass('active');
              $('#one').hide();
              $('#two').hide();
              $('#three').hide();
              $('#four').hide();
              $('#five').show();
          });

     });
  </script>
@endsection
@section('content')
  <div class="container-fluid m-0 p-0" style="background-color:white">
      <div class="row">
          <div class="col-md-9 col-sm-9 col-xl-9 m-0 p-0">
             <div class="row m-1" id="one" style="">
                 <div class="col-12" style="height:640px">
                    {{-- <video id="myPlayer" controls playsInline webkit-playsinline autoplay style="width:100%;">
                        <source src="rtsp://admin:admin@123@192.168.1.108:554/cam/realmonitor?channel=2&subtype=1" type="rtsp/flv" />
                    </video> --}}

                    {{-- <div id="myVideo" style="width:320px;height:240px;border: solid 1px"></div>
                    <input type="button" onclick="connect()" value="PLAY"/> --}}


                    {{-- <iframe id='fp_embed_player' src='https://demo.flashphoner.com:8888/embed_player?urlServer=wss://demo.flashphoner.com:8443&streamName=rtsp://admin:admin@123@192.168.1.108/cam/realmonitor?channel=1%26subtype=1&mediaProviders=WebRTC,Flash,MSE,WSPlayer' marginwidth='0' marginheight='0' frameborder='0' width='100%' height='100%' scrolling='no' allowfullscreen='allowfullscreen'></iframe> --}}

                    <object classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921"
                        codebase="http://download.videolan.org/pub/videolan/vlc/last/win32/axvlc.cab"
                        style= "width: 200px; height: 200px;"
                    >
                        <param name="src" value="rtsp://admin:admin@123@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1" />
                        <embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org"
                            width="800px"
                            height="640px"
                            src="rtsp://admin:admin@123@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1"
                        />
                    </object>

                 </div>
             </div>
            <div class="row m-0 p-0" id="two"  style="display:none">
                <div class="col-6 " >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6 " >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6 " >
                    <div class=" m-1 " style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6" >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
                <div class="col-6" >
                    <div class=" m-1" style="display: block;width:100%;height:400px;background-color:gray">

                    </div>
                </div>
            </div>
            <div class="row m-1" id="three"  style="display:none">
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
                <div class="col-4" >
                    <div class=" m-1" style="display: block;width:100%;height:280px;background-color:gray">

                    </div>
                </div>
            </div>
            <div class="row m-1" id="four"  style="display:none">
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
                <div class="col-3" >
                    <div class=" m-1" style="display: block;width:100%;height:200px;background-color:gray">

                    </div>
                </div>
            </div>
          </div>
          <div class="col-md-3 m-0 p-0">
            @include('admin.include.rsidebar')
          </div>
      </div>
  </div>


@endsection
@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
<script src="{{asset('public/js/pictarea.js')}}"></script>
  {{-- <script src="{{asset('public/js/stream/webVideoCtrl.js')}}"></script> --}}
  {{-- <script src="{{asset('public/js/stream/hikapi.js')}}"></script> --}}
  {{-- <script src="{{asset('public/js/stream/monitor.js')}}"></script> --}}
  {{-- <script src="https://open.ys7.com/sdk/js/2.0/ezuikit.js"></script> --}}
<script>
    $(function() {


   function log(str){
       var div = document.createElement('DIV');
       div.innerHTML = (new Date()).Format('yyyy-MM-dd hh:mm:ss.S') + JSON.stringify(str);
       document.body.appendChild(div);
   }


    var $tabButtonItem = $('#tab-button li'),
        $tabSelect = $('#tab-select'),
        $tabContents = $('.tab-contents'),
        activeClass = 'is-active';

    $tabButtonItem.first().addClass(activeClass);
    $tabContents.not(':first').hide();

    $tabButtonItem.find('a').on('click', function(e) {
        var target = $(this).attr('href');

        $tabButtonItem.removeClass(activeClass);
        $(this).parent().addClass(activeClass);
        $tabSelect.val(target);
        $tabContents.hide();
        $(target).show();
        e.preventDefault();
    });

    $tabSelect.on('change', function() {
        var target = $(this).val(),
            targetSelectNum = $(this).prop('selectedIndex');

        $tabButtonItem.removeClass(activeClass);
        $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
        $tabContents.hide();
        $(target).show();
    });
    });

  $('#html').jstree();

  </script>
@endsection
