@extends('client.include.layout')

@section('css')
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeq9Qkxz_Dvu_B3Qe_J8YZQdC5jaIEsF8&callback=initMap" async defer></script>
  <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
  </script>
@endsection

@section('content')
   <div class="container-fluid m-0 bg-light tb-0" style="height:100%;">
       <div class="row" style="height:100%;">
        <div id="map" style="height:640px;width:100%;"></div>


       </div>
   </div>
@endsection

@section('script')

@endsection
