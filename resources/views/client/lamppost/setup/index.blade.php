@extends('admin.include.layout')

@section('title')
@endsection
@section('css')
   <style>
       .master-div{
           border : 1px solid gray;
           padding : 5px;
           border-radius : 5px;
       }


       .setup-card{
           border :  1px solid gray;
           border-radius : 5px;
           margin : 5px;
           padding: 10px;
       }


   </style>
@endsection

@section('content')
  <div class="container-fluid">
      <div class="row">
        <div class="col-12 mb-3">
            <h2 class="text-center text-warning"> All Lamppost setup configuration</h2>
        </div>

        <div class="col-8 offset-sm-2">
            <div class="master-div mb-3">
                <div class="row">
                    <div class="col-4">
                        <h4 class="form-cntrol-label">LAMPPOST Master IP</h4>
                    </div>
                    <div class="col-5">
                        <input type="text" name="lamppost_master_ip" id="lamppost_master_ip" class="form-control">
                    </div>
                    <div class="col-3 text-center">
                        <button type="submit" class="btn btn-success" name="ptz_submit">submit</button>
                    </div>
                </div>
            </div>
        </div>


         <div class="col-4">
           <div class="setup-card">
               <form action="" method="post">
                   <h4 class="text-center text-dark mb-3"><i class="fas fa-camera"></i>&nbsp;PTZ Camera Setup</h4>
                   <div class="form-group row">
                        <label for="inputIp" class="col-sm-4 col-form-label">IP Address</label>
                        <div class="col-sm-8">
                        <input type="ptz_ip_address" class="form-control" id="inputIp" name="ptz_ip" placeholder="Enter ptz camera ip address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPtzUsername" class="col-sm-4 col-form-label">Username</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control"  name="ptz_username" id="inputPtzUsername" placeholder="Enter username for ptz camera">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPtzPassword" class="col-sm-4 col-form-label">Password</label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="inputPtzPassword" name="ptz_password" placeholder="Enter password for ptz camera">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputSubmit" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-success" name="ptz_submit">&nbsp;submit</button>
                        </div>
                    </div>
               </form>
           </div>
         </div>

         <div class="col-4">
            <div class="setup-card">
                <form action="" method="post" >
                    <h4 class="text-center text-dark mb-3"><i class="fas fa-desktop"></i></i>&nbsp;LED Screen Setup</h4>
                    <div class="form-group row">
                         <label for="inputLedIp" class="col-sm-4 col-form-label">IP Address</label>
                         <div class="col-sm-8">
                         <input type="led_ip_address" class="form-control" id="inputLedIp" placeholder="Enter led screen ip address">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputLedUsername" class="col-sm-4 col-form-label">Username</label>
                         <div class="col-sm-8">
                         <input type="led_username" class="form-control" id="inputLedUsername" placeholder="Enter username for led screen">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputLedPassword" class="col-sm-4 col-form-label">Password</label>
                         <div class="col-sm-8">
                         <input type="password" class="form-control" id="inputLedPassword" placeholder="Enter password for led screen">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputLedSubmit" class="col-sm-4 col-form-label"></label>
                         <div class="col-sm-8">
                             <button type="submit" class="btn btn-success"><i class="fa fa-user" name="screen_submit"></i>&nbsp;submit</button>
                         </div>
                     </div>

                </form>
            </div>
          </div>

          <div class="col-4">
            <div class="setup-card">
                <form action="" method="post">
                    <h4 class="text-center text-dark mb-3"><i class="fa fa-fire"></i></i>&nbsp;Sensor Setup</h4>
                    <div class="form-group row">
                         <label for="inputSensorIp" class="col-sm-4 col-form-label">IP</label>
                         <div class="col-sm-8">
                         <input type="text" class="form-control" id="inputSensorIp" name="sensor_ip_address" placeholder="Enter sensor ip address">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSensorUsername" class="col-sm-4 col-form-label">Username</label>
                         <div class="col-sm-8">
                         <input type="text" name="sensor_username" class="form-control" id="inputSensorUsername" placeholder="Enter username for sensor">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSensorPassword" class="col-sm-4 col-form-label">Password</label>
                         <div class="col-sm-8">
                         <input type="password" class="form-control" id="inputSensorPassword"  name="sensor_password" placeholder="Enter password for ptz camera">
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputSensorSubmit" class="col-sm-4 col-form-label"></label>
                         <div class="col-sm-8">
                             <button type="submit" class="btn btn-success"><i class="fa fa-user" name="sensor_submit"></i>&nbsp;submit</button>
                         </div>
                     </div>

                </form>
            </div>
          </div>


      </div>
  </div>
@endsection

@section('script')
@endsection
