@extends('admin.include.layout')

@section('css')
<link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
<link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
<style type="text/css">
    /* .jstree li > a > .jstree-icon {  "background-image":url("{{ asset('public/tree/lamp.png') }}") !important; } */


</style>
@endsection

@section('content')
   <div class="container-fluid m-0 bg-light tb-0">
       <div class="row">
        <div class="col-2 border-right m-0 p-0 border-bottom" style="height:700px;">
        {{-- tree view --}}
        <div id="html" class="demo mt-2">
            <ul>
            <li data-jstree='{ "opened" : true,"icon":"{{asset('public/tree/lamp.png')}}"}'>Lamppost
                    <ul>
                        <li data-jstree='{ "selected" : true }'>Camera 1</li>
                        <li>Camera 2</li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        <div class="col-10 m-0 p-0 border-bottom" >
             {{-- write here --}}
        </div>
       </div>
   </div>
@endsection

@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
<script>
    $('#html').jstree();
    $("#html i.jstree-icon").click(function(){
    $(this).css("backgroundColor", "white");
});
    // $("#html").jstree().set_icon(nodeId, "{{ asset('public/tree/lamp.png') }}");
</script>
@endsection
