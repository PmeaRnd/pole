@extends('admin.include.layout')

@section('css')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <style>


      .modal .modal-bottom .modal-dialog {
    width:20%;
    position:fixed;
    bottom:0;
    right:0;
    margin:0;
}

      .tab-pane{
          margin:10px;
      }

      .slidecontainer {
        width: 100%;
      }

.slider {
  -webkit-appearance: none;
  width: 100%;
  height: 15px;
  border-radius: 5px;
  background: #d3d3d3;
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: #4CAF50;
  cursor: pointer;
}
  </style>
@endsection

@section('content')
   <div class="container-fluid m-0 bg-light tb-0">
       <div class="row">
        <div class="col-2 border-right m-0 p-0 border-bottom" style="height:700px;">

        </div>
        <div class="col-10 m-0 p-0 border-bottom" >
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-bright-tab" data-toggle="tab" href="#nav-bright" role="tab" aria-controls="nav-bright" aria-selected="true">Display Brightness</a>
                  <a class="nav-item nav-link" id="nav-volume-tab" data-toggle="tab" href="#nav-volume" role="tab" aria-controls="nav-volume" aria-selected="false">Volume Control</a>
                  <a class="nav-item nav-link" id="nav-switch-tab" data-toggle="tab" href="#nav-switch" role="tab" aria-controls="nav-switch" aria-selected="false">Display Switch</a>
                  <a class="nav-item nav-link" id="nav-ir-tab" data-toggle="tab" href="#nav-ir" role="tab" aria-controls="nav-ir" aria-selected="false">IR Switch</a>
                  <a class="nav-item nav-link " id="nav-reboot-tab" data-toggle="tab" href="#nav-reboot" role="tab" aria-controls="nav-reboot" aria-selected="true">System Reboot Brightness</a>
                  <a class="nav-item nav-link" id="nav-update-tab" data-toggle="tab" href="#nav-update" role="tab" aria-controls="nav-update" aria-selected="false">Software Update</a>
                  <a class="nav-item nav-link" id="nav-info-tab" data-toggle="tab" href="#nav-info" role="tab" aria-controls="nav-info" aria-selected="false">Real Time Info Switch</a>

              </nav>
              <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-bright" role="tabpanel" aria-labelledby="nav-bright-tab">
                    <div class="row">
                        <div class="col-6">
                            <div class="slidecontainer">
                                <input type="range" min="1" max="64" value="23" class="slider" id="myRange">
                                <p>Value: <span id="demo"></span></p>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">BRIGHTNESS</span><span> List of scheduled tasks</span>
                                  <button class="btn btn-sm btn-primary float-right" id="new_task">New Task</button>
                                </div>
                                <div class="card-body" style="height:300px;">
                                    <table class="table table-sm table-bordered table-hovered">
                                        <thead>
                                            <tr class="text-center">
                                                <th></th>
                                                <th>Name</th>
                                                <th>New Time</th>
                                                <th>Created BY</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card" id="add_new_task" style="display:none">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">Task Information</span>
                                </div>
                                <div class="card-body" style="height:auto;">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group row">
                                                <label for="inputName" class="col-sm-4 col-form-label">Task Name</label>
                                                <div class="col-sm-8">
                                                  <input type="text" class="form-control" id="inputName" placeholder="Name">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="inputDefault" class="col-sm-6 col-form-label">Default Brightness</label>
                                                <div class="col-sm-6">
                                                  <input type="number" class="form-control" id="inputDefault" placeholder="">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="float-right">
                                                <button class="btn btn-primary btn-sm">save</button>
                                                <button class="btn btn-dark btn-sm" id="btn-add-row"><i class="fa fa-plus"></i>&nbsp; Add</button>
                                            </div>
                                        </div>
                                        <div class="col-12" id="row-container">
                                           <div class="row border p-1 mt-2" id="add_row" data-id="rec-1">
                                               <div class="col-4">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-6 col-form-label">Brightness</label>
                                                    <div class="col-sm-6">
                                                      <input type="password" class="form-control" id="inputPassword" placeholder="">
                                                    </div>
                                                  </div>
                                               </div>
                                               <div class="col-8">
                                                   <button class="btn btn-sm btn-danger float-right" id="btn-delete" data-id="delete-1"><i class="fa fa-trash" ></i>&nbsp;Delete</button>
                                               </div>
                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                  <input type="checkbox" name="range" id="range"> Date Range :
                                               </div>
                                               <div class="col-2 mb-2">
                                                   <input type="text" name="from_date" id="from_date" class="form-control">
                                               </div>
                                               <div class="col-2 mb-2">
                                                <input type="text" name="to_date" id="to_date" class="form-control">
                                               </div>
                                               <div class="col-6"></div>

                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                <input type="checkbox" name="frame" id="frame">Time Frame :
                                                </div>
                                                <div class="col-2 mb-2">
                                                    <input type="text" name="from_time" id="from_time" class="form-control">
                                                </div>
                                                <div class="col-2 mb-2">
                                                <input type="text" name="to_time" id="to_time" class="form-control">
                                                </div>
                                                <div class="col-6 mb-2"></div>
                                                <div class="col-12 mb-2" style="font-weight:bold">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" id="inlineWeek" value="week">
                                                        <label class="form-check-label" for="inlineWeek">Specify the week:</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineMonday" value="monday">
                                                        <label class="form-check-label" for="inlineMonday">Monday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineTuesday" value="tuesday">
                                                        <label class="form-check-label" for="inlineTuesday">Tuesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineWednesday" value="wednesday">
                                                        <label class="form-check-label" for="inlineWednesday">Wednesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineThursday" value="thursday">
                                                        <label class="form-check-label" for="inlineThursday">Thursday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineFriday" value="friday">
                                                        <label class="form-check-label" for="inlineFriday">Friday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineSaturday" value="saturday">
                                                        <label class="form-check-label" for="inlineSaturday">Saturday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="inlineSunday" value="sunday">
                                                        <label class="form-check-label" for="inlineSunday">Sunday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                      </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="nav-volume" role="tabpanel" aria-labelledby="nav-volume-tab">
                    <div class="row">
                        <div class="col-6">
                            <div class="slidecontainer">
                                <input type="range" min="1" max="64" value="23" class="slider" id="v_range" >
                                <p>Manual Value: <span id="v_range_value"></span></p>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">VOLUME</span><span> List of scheduled tasks</span>
                                  <button class="btn btn-sm btn-primary float-right" id="v_new_task">New Task</button>
                                </div>
                                <div class="card-body" style="height:300px;">
                                    <table class="table table-sm table-bordered table-hovered">
                                        <thead>
                                            <tr class="text-center">
                                                <th></th>
                                                <th>Name</th>
                                                <th>New Time</th>
                                                <th>Created BY</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card" id="v_add_new_task" style="display:none">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">Task Information</span>
                                </div>
                                <div class="card-body" style="height:auto;">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group row">
                                                <label for="inputVName" class="col-sm-4 col-form-label">Task Name</label>
                                                <div class="col-sm-8">
                                                  <input type="text" class="form-control" id="inputVName" placeholder="Name" name="v_name">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="inputVDefault" class="col-sm-6 col-form-label">Default Volume</label>
                                                <div class="col-sm-6">
                                                  <input type="number" class="form-control" id="inputVDefault" placeholder="" name="v_default">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="float-right">
                                                <button class="btn btn-primary btn-sm v_save">save</button>
                                                <button class="btn btn-dark btn-sm" id="v_btn-add-row"><i class="fa fa-plus"></i>&nbsp; Add</button>
                                            </div>
                                        </div>
                                        <div class="col-12" id="v_row-container">
                                           <div class="row border p-1 mt-2" id="v_add_row" data-id="v_rec-1">
                                               <div class="col-4">
                                                <div class="form-group row">
                                                    <label for="inputVolume" class="col-sm-6 col-form-label">Volume</label>
                                                    <div class="col-sm-6">
                                                      <input type="text" class="form-control" id="inputVolume" placeholder="" name="volume">
                                                    </div>
                                                  </div>
                                               </div>
                                               <div class="col-8">
                                                   <button class="btn btn-sm btn-danger float-right" id="v_btn-delete" data-id="v_delete-1"><i class="fa fa-trash" ></i>&nbsp;Delete</button>
                                               </div>
                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                  <input type="checkbox" name="v_range" id="v_range"> Date Range :
                                               </div>
                                               <div class="col-2 mb-2">
                                                   <input type="text" name="v_from_date" id="v_from_date" class="form-control">
                                               </div>
                                               <div class="col-2 mb-2">
                                                <input type="text" name="v_to_date" id="v_to_date" class="form-control">
                                               </div>
                                               <div class="col-6"></div>

                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                <input type="checkbox" name="v_frame" id="v_frame">Time Frame :
                                                </div>
                                                <div class="col-2 mb-2">
                                                    <input type="text" name="v_from_time" id="v_from_time" class="form-control">
                                                </div>
                                                <div class="col-2 mb-2">
                                                <input type="text" name="v_to_time" id="v_to_time" class="form-control">
                                                </div>
                                                <div class="col-6 mb-2"></div>
                                                <div class="col-12 mb-2" style="font-weight:bold">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" id="vinlineWeek" value="week" name="v_day">
                                                        <label class="form-check-label" for="vinlineWeek">Specify the week:</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineMonday" value="monday" name="v_monday">
                                                        <label class="form-check-label" for="vinlineMonday">Monday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineTuesday" value="tuesday" name="v_tuesday">
                                                        <label class="form-check-label" for="inlineTuesday">Tuesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineWednesday" value="wednesday" name="v_wednesday">
                                                        <label class="form-check-label" for="vinlineWednesday">Wednesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineThursday" value="thursday" name="v_thusaday">
                                                        <label class="form-check-label" for="vinlineThursday">Thursday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineFriday" value="friday" name="v_frinday">
                                                        <label class="form-check-label" for="vinlineFriday">Friday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineSaturday" value="saturday" name="v_saturday">
                                                        <label class="form-check-label" for="vinlineSaturday">Saturday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineSunday" value="sunday" name="v_sunday">
                                                        <label class="form-check-label" for="vinlineSunday">Sunday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                      </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="nav-switch" role="tabpanel" aria-labelledby="nav-switch-tab">
                    <div class="row">
                        <div class="col-6 mb-3">
                            MANUAL : <input type="checkbox" checked data-toggle="toggle">
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">VOLUME</span><span> List of scheduled tasks</span>
                                  <button class="btn btn-sm btn-primary float-right" id="sw_new_task">New Task</button>
                                </div>
                                <div class="card-body" style="height:300px;">
                                    <table class="table table-sm table-bordered table-hovered">
                                        <thead>
                                            <tr class="text-center">
                                                <th></th>
                                                <th>Name</th>
                                                <th>New Time</th>
                                                <th>Created BY</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card" id="sw_add_new_task" style="display:none">
                                <div class="card-header">
                                  <span style="font-size:20px;font-weight:bold;color:#007BFF">Task Information</span>
                                </div>
                                <div class="card-body" style="height:auto;">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group row">
                                                <label for="inputVName" class="col-sm-4 col-form-label">Task Name</label>
                                                <div class="col-sm-8">
                                                  <input type="text" class="form-control" id="inputVName" placeholder="Name" name="sw_name">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="inputVDefault" class="col-sm-6 col-form-label">Default Volume</label>
                                                <div class="col-sm-6">
                                                  <input type="number" class="form-control" id="inputVDefault" placeholder="" name="sw_default">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="float-right">
                                                <button class="btn btn-primary btn-sm sw_save">save</button>
                                                <button class="btn btn-dark btn-sm" id="sw_btn-add-row"><i class="fa fa-plus"></i>&nbsp; Add</button>
                                            </div>
                                        </div>
                                        <div class="col-12" id="sw_row-container">
                                           <div class="row border p-1 mt-2" id="sw_add_row" data-id="sw_rec-1">
                                               <div class="col-4">
                                                <div class="form-group row">
                                                    <label for="inputVolume" class="col-sm-6 col-form-label">Volume</label>
                                                    <div class="col-sm-6">
                                                      <input type="text" class="form-control" id="inputVolume" placeholder="" name="volume">
                                                    </div>
                                                  </div>
                                               </div>
                                               <div class="col-8">
                                                   <button class="btn btn-sm btn-danger float-right" id="sw_btn-delete" data-id="sw_delete-1"><i class="fa fa-trash" ></i>&nbsp;Delete</button>
                                               </div>
                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                  <input type="checkbox" name="sw_range" id="sw_range"> Date Range :
                                               </div>
                                               <div class="col-2 mb-2">
                                                   <input type="text" name="sw_from_date" id="sw_from_date" class="form-control">
                                               </div>
                                               <div class="col-2 mb-2">
                                                <input type="text" name="sw_to_date" id="sw_to_date" class="form-control">
                                               </div>
                                               <div class="col-6"></div>

                                               <div class="col-2 mb-2" style="font-weight:bold">
                                                <input type="checkbox" name="sw_frame" id="sw_frame">Time Frame :
                                                </div>
                                                <div class="col-2 mb-2">
                                                    <input type="text" name="sw_from_time" id="sw_from_time" class="form-control">
                                                </div>
                                                <div class="col-2 mb-2">
                                                <input type="text" name="sw_to_time" id="sw_to_time" class="form-control">
                                                </div>
                                                <div class="col-6 mb-2"></div>
                                                <div class="col-12 mb-2" style="font-weight:bold">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" id="vinlineWeek" value="week" name="sw_day">
                                                        <label class="form-check-label" for="vinlineWeek">Specify the week:</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineMonday" value="monday" name="sw_monday">
                                                        <label class="form-check-label" for="vinlineMonday">Monday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineTuesday" value="tuesday" name="sw_tuesday">
                                                        <label class="form-check-label" for="inlineTuesday">Tuesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineWednesday" value="wednesday" name="sw_wednesday">
                                                        <label class="form-check-label" for="vinlineWednesday">Wednesday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineThursday" value="thursday" name="sw_thusaday">
                                                        <label class="form-check-label" for="vinlineThursday">Thursday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineFriday" value="friday" name="sw_frinday">
                                                        <label class="form-check-label" for="vinlineFriday">Friday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineSaturday" value="saturday" name="sw_saturday">
                                                        <label class="form-check-label" for="vinlineSaturday">Saturday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input class="form-check-input" type="checkbox" id="vinlineSunday" value="sunday" name="sw_sunday">
                                                        <label class="form-check-label" for="vinlineSunday">Sunday</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                      </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-ir" role="tabpanel" aria-labelledby="nav-ir-tab">
                    <div class="row">

                        <div class="col-3 text-center">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customNot" name="ir_on">
                                <label class="custom-control-label" for="customNot">Not</label>
                            </div>
                        </div>
                        <div class="col-3 text-center">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customIrOpen" name="ir_on">
                                <label class="custom-control-label" for="customIrOpen">IR Open and Display</label>
                            </div>
                        </div>
                        <div class="col-3 text-center">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customIrOff" name="ir_on">
                                <label class="custom-control-label" for="customIrOff">IR Off Display</label>
                            </div>
                        </div>
                        <div class="col-1 text-center">
                            <div class="form-group row">
                                <div class="col-sm-8">
                                  <input type="number" class="form-control form-control-sm" id="secLbl">
                                </div>
                                <label for="secLbl" class="col-sm-4 col-form-label col-form-label-sm">S</label>
                              </div>
                        </div>
                        <div class="col-1 text-center">
                            <button type="submit" class="btn btn-primary btn-sm">send</button>
                        </div>
                        <div class="col-1"></div>
                        <div class="col-10 text-center">
                           <span style="font-weight:bold;">Search :&nbsp; </span>
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#irModal"><i class="fa fa-search"></i>Check IR Status</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-reboot" role="tabpanel" aria-labelledby="nav-reboot-tab">
                    <div class="row">
                        <div class="col-2">
                            <div class="form-check">
                                <input class="form-check-input " type="checkbox" value="" id="reboot">
                                <label class="form-check-label " for="reboot">
                                  Schedule Reboot
                                </label>
                              </div>
                        </div>
                        <div class="col-2">
                           <input type="text" name="" id="" class="form-control">
                        </div>
                        <div class="col-2">
                            <button class="btn btn-primary">submit</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-update" role="tabpanel" aria-labelledby="nav-update-tab">
                   <div class="row">
                       <div class="col-8">
                           <table class="table table-bordered table-striped table-sm">
                               <thead class="text-center">
                                    <tr>
                                        <td></td>
                                        <td>Version File</td>
                                    </tr>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td>1</td>
                                       <td>145.zip</td>
                                   </tr>
                                   <tr>
                                        <td>2</td>
                                        <td>150.zip</td>
                                   </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>jwPlayer403-20180502.zip</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>jwPlayer403-20180509.zip</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>jwPlayer403-20180517Alarm.zip</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>jwPlayer403.zip</td>
                                    </tr>
                               </tbody>
                           </table>
                       </div>
                   </div>
                </div>
                <div class="tab-pane fade" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
                  <div class="row">
                      <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="float-right">
                                    <button class="btn btn-primary">start the batch</button>
                                    <button class="btn btn-danger">batch quest</button>
                                    <button class="btn btn-success">batch configuration</button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-bordered table-striped ">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>
                                                <input type="checkbox" name="" id="">
                                            </th>
                                            <th>Name</th>
                                            <th>Width</th>
                                            <th>Height</th>
                                            <th>Tunnel</th>
                                            <th>SN</th>
                                            <th>IP Address</th>
                                            <th>Companies</th>
                                            <th>Port</th>
                                            <th>Host Server Supplier</th>
                                            <th>Host Server Model</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <input type="checkbox" name="" id="">
                                            </td>
                                            <td>60n v</td>
                                            <td>120</td>
                                            <td>100</td>
                                            <td>Tunnel</td>
                                            <td>y10-817-80865</td>
                                            <td>IP Address</td>
                                            <td>Companies</td>
                                            <td>0</td>
                                            <td>Sysolution</td>
                                            <td>Host Server Model</td>
                                            <td>Start</td>
                                            <td>

                                                <button class="btn btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
        </div>


        <!-- Modal -->
        <div class="modal modal-bottom fade" id="irModal" tabindex="-1" role="dialog" aria-labelledby="irModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="irModalLabel">Warning Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    17:37:50 Please select the device <br>
                    17:37:50 Please select the device
                </div>

            </div>
            </div>
        </div>
   </div>
@endsection

@section('script')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <script >

      $(function(){
        var slider = document.getElementById("myRange");
        var output = document.getElementById("demo");
        output.innerHTML = slider.value;

        slider.oninput = function() {
        output.innerHTML = this.value;
        }

        var  v_slider = document.getElementById("v_range");
        var v_output = document.getElementById("v_range_value");
        v_output.innerHTML = v_slider.value;

        v_slider.oninput = function() {
            v_output.innerHTML = this.value;
        }




        $('#new_task').click(function(){
           $('#add_new_task').toggle();
        });

        $('#btn-add-row').click(function(){
           var copy_data = $('#add_row').clone();

           var size = $('#add_row').length + 1;
           copy_data.attr('data-id', 'rec-'+size);
           copy_data.find('#btn-delete').attr('data-id', 'delete-'+size);
           $('#row-container').append(copy_data);
        });

        $(document).on('click','#btn-delete',function(){

           var str = $(this).attr('data-id');
           var id = str.replace("delete", "rec");
           console.log(id);
        //    $(id).remove();
          $("div[data-id='" + id + "']").remove();
        $(document).find(id).remove();
           $('rec-1').remove();
        });



        // volume

        $('#v_new_task').click(function(){
           $('#v_add_new_task').toggle();
        });

        $('#v_btn-add-row').click(function(){
           var copy_data = $('#v_add_row').clone();

           var size = $('#v_add_row').length + 1;
           copy_data.attr('data-id', 'v_rec-'+size);
           copy_data.find('#v_btn-delete').attr('data-id', 'v_delete-'+size);
           $('#v_row-container').append(copy_data);
        });

        $(document).on('click','#v_btn-delete',function(){

           var str = $(this).attr('data-id');
           var id = str.replace("v_delete", "v_rec");
          $("div[data-id='" + id + "']").remove();
        $(document).find(id).remove();
           $('rec-1').remove();
        });


         // volume

         $('#sw_new_task').click(function(){
           $('#sw_add_new_task').toggle();
        });

        $('#sw_btn-add-row').click(function(){
           var copy_data = $('#sw_add_row').clone();

           var size = $('#sw_add_row').length + 1;
           copy_data.attr('data-id', 'sw_rec-'+size);
           copy_data.find('#sw_btn-delete').attr('data-id', 'sw_delete-'+size);
           $('#sw_row-container').append(copy_data);
        });

        $(document).on('click','#sw_btn-delete',function(){

           var str = $(this).attr('data-id');
           var id = str.replace("sw_delete", "sw_rec");
          $("div[data-id='" + id + "']").remove();
        $(document).find(id).remove();
           $('rec-1').remove();
        });

      });
  </script>
@endsection
