@extends('admin.include.layout')

@section('css')
<link rel="stylesheet" href="{{asset('public/css/rsidebar.css')}}">
<link rel="stylesheet" href="{{asset('public/tree/dist/themes/default/style.min.css')}}">
@endsection

@section('content')
   <div class="container-fluid m-0 bg-light tb-0">
       <div class="row">
        <div class="col-2 border-right m-0 p-0 border-bottom" style="height:700px;">
        {{-- tree view --}}
        <div id="html" class="demo mt-2">
            <ul>
                <li data-jstree='{ "opened" : true }'>Lamppost
                    <ul>
                        <li data-jstree='{ "selected" : true }'>Camera 1</li>
                        <li>Camera 2</li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        <div class="col-10 m-0 p-0 border-bottom" >
         <div class="card" style="height:100%">
             <div class="card-header">
               <div>
                <button class="btn btn-primary" data-toggle="modal" data-target=".add_group"><i class="fa fa-plus"></i>&nbsp;  Add Group</button>&nbsp;&nbsp;
                <button class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp; Delete Group</button>
                <button class="btn btn-primary float-right" data-toggle="modal" data-target="#add-pole"><i class="fa fa-plus"></i>&nbsp; Add Pole</button>
               </div>
             </div>
             <div class="card-body" style="height:100%">

             </div>
           </div>
        </div>
       </div>

        <div class="modal fade add_group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                      Input Group Name
                    </div>
                    <div class="card-body">
                       <input type="text" name="group_name" id="group_name" class="form-control">
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
            </div>
        </div>
        </div>


        {{-- Add Pole --}}
        <!-- Modal -->
        <div class="modal fade" id="add-pole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLabel">Select Light Pole</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                   <table class="table table-bordered table-hovered">
                        <thead class="text-center">
                            <tr>
                                <th>
                                    <input type="checkbox" name="" id="">
                                </th>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                   </table>
                </div>
                <div class="modal-footer">

                <button type="button" class="btn btn-primary">Sure</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            </div>
        </div>
   </div>
@endsection

@section('script')
<script src="{{asset('public/tree/dist/jstree.min.js')}}"></script>
<script>
    $('#html').jstree();
</script>
@endsection
