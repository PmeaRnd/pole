@extends('admin.include.layout')
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="content">
<div class="container-fluid"><br>
    <p class="mt-1">
        <div class="row">
        <div class="col-md-8 mb-2">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date"  />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date"  readonly/>
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
            </div>
        </div>
        <div class="col-md-4 float-right mb-2">
            <a href="{{route('user.create')}}" class="btn btn-danger "> <i class="fa fa-trash"></i>&nbsp;Delete</a>
            <button class="btn btn-primary " id="new-btn" data-toggle="modal" data-target="#browse"> <i class="fa fa-plus"></i>&nbsp;New</button>
        </div>
    </div>
    </p>
    <table class="table table-sm table-bordered data-table table-hover ">
        <thead class="bg-dark">
            <tr class="text-center">
                <th>#</th>
                <th>
                    <input type="checkbox">
                </th>
                <th>Name</th>
                <th>Preview</th>
                <th>Type</th>
                <th>Tag</th>
                <th>Uploader By</th>
                <th>Upload Time</th>
                <th width="150px" class="text-center">Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
        </tbody>
    </table>
    <!-- Modal -->
<div class="modal fade" id="browse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Choose Media File to upload</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <!-- Propeller Filled File Input Inverse -->
            <div class="card pmd-card bg-dark text-white">
                <div class="card-body">
                    <!-- File Input -->
                    <form action="" method="post" id="upload_form">
                        <div class="form-group">
                            <div class="custom-file pmd-custom-file-filled">
                                <input type="file" class="custom-file-input" id="inverse_customfilledFile" name="media_upload_file" accept="audio/*|video/*|image/*">
                                <label class="custom-file-label" for="inverse_customfilledFile">Choose file</label>
                            </div>
                        </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">upload</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">cancel</button>
        </form>
        </div>
      </div>
    </div>
  </div>

  {{-- close modal --}}
</div>
</section>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
     $('#upload_form').submit(function(event){
         event.preventDefault();
         var  upload_file = $('#inverse_customfilledFile').val();
         if(upload_file !== ''){
            var file_data = $('#inverse_customfilledFile').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            // alert(form_data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('medias.store')}}", // point to server-side PHP script
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    $("#browse").hide();
                                toastr.success('Success', response.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });

                },
                error: function(err){
                                toastr.error('Success', 'Media upload failed.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                }
            });
         }
         else{
            toastr.error('Success', 'Media should be audio/video file.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
         }
     });
  });


  $(function () {
    var options={
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd hh:mm:ss',
        todayHighlight: true,
        autoclose: true,
      };
    $('#from_date').datetimepicker(options);

    $('#from_date').change(function(){
        var from = $(this).val();
        if(from !== ''){
            $('#to_date').datetimepicker(options);
           $('#to_date').removeAttr('readonly');
        }
        else{
            $('#to_date').attr('readonly',true);
        }
    })
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('medias.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'media', name: 'media', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'snap', name: 'snap'},
            {data: 'type', name: 'type'},
            {data: 'tag', name: 'tag'},
            {data: 'upload_by', name: 'upload_by'},
            {data: 'upload_time', name: 'upload_time'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click','#delete-btn',function(){
        var id = $(this).attr('data-id');
         bootbox.confirm({
            title: '<i class="fa fa-trash fa-lg"></i>Delete Record?',
            message: "Do you want to delete it or not.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if(result)
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url : "{{url('admin/lamppost/medias')}}"+"/"+id,
                        type: "DELETE",
                        data : {},
                        success: function(data, textStatus, jqXHR)
                        {
                            console.info(data);
                            console.log(textStatus);
                            console.log(jqXHR);
                            if(textStatus == 'success'){
                                toastr.success('Success', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                            else{
                                toastr.error('Error', data.message,{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            console.info(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            toastr.error('Error', 'Internal error occur.',{
                                showMethod: 'fadeIn',
                                showDuration: 300,
                                showEasing: 'swing',
                                hideMethod: 'fadeOut',
                                hideDuration: 1000,
                                hideEasing: 'swing',
                                extendedTimeOut: 1000,
                                positionClass: 'toast-top-center',
                                timeOut: 3000,
                                escapeHtml: false,
                                newestOnTop: true,
                                preventDuplicates: false,
                                progressBar: true
                                });
                        }
                    });
                }
            }
        });
    })

    $(document).on('click','#edit-btn',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/medias')}}"+"/"+id,
            type: "GET",
            data : {},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var url = "{{asset('public/media')}}";
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">Change Media Info</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        setTimeout(function(){
                            dialog.find('.bootbox-body').html(`
                            <form id="update_media_info" action="#" method="post">
                            <input type="hidden" name="media_id" value="`+data.data.id+`" id="media_id">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputName" placeholder="Name" value="`+data.data.name+`" name="media_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputTag" class="col-sm-2 col-form-label">Tag</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputTag" placeholder="Tag" value="`+data.data.tag+`" name="media_tag">
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-2"></div>
                              <div class="col-md-10">
                                <button type="submit" class="btn btn-primary">update</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">cancel</button>
                              </div>
                            </div>
                            </form>`);
                        }, 500);
                    });
                }
                else{
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    $(document).on('click','img#media_img',function(){
       var id = $(this).attr('data-id');
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/media')}}",
            type: "POST",
            data : {id:id},
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                    var url = "{{asset('public/media')}}";
                    var dialog = bootbox.dialog({
                        title: '<div style="align-text:center;">Media File</div>',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });

                    dialog.init(function(){
                        console.log(data);
                        setTimeout(function(){
                            var url = "{{asset('public/media')}}"+"/"+data.data.media;
                            dialog.find('.bootbox-body').html(`
                            <video width="100%" height="100%" controls>
                               <source src="`+url+`" type="video/mp4">
                               Your browser does not support the video tag.
                            </video>`);
                        }, 500);
                    });
                }
                else{
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

   $(document).on('submit','#update_media_info',function(event){
       event.preventDefault();
       var me = $(this);
       var id = $('#media_id').val();
       $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : "{{url('admin/lamppost/medias')}}"+"/"+id,
            type: "PUT",
            data : $('#update_media_info').serialize(),
            success: function(data, textStatus, jqXHR)
            {
                if(textStatus == 'success'){
                   $('.bootbox').hide();

                   toastr.success('Success',data.message,{
                        showMethod: 'fadeIn',
                        showDuration: 300,
                        showEasing: 'swing',
                        hideMethod: 'fadeOut',
                        hideDuration: 1000,
                        hideEasing: 'swing',
                        extendedTimeOut: 1000,
                        positionClass: 'toast-top-center',
                        timeOut: 3000,
                        escapeHtml: false,
                        newestOnTop: true,
                        preventDuplicates: false,
                        progressBar: true
                        });
                }
                else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {}
        });
    });

    @if(\Session::has('user_edit_success'))
        toastr.success('Success',"{{\Session::get('user_edit_success')}}",{
        showMethod: 'fadeIn',
        showDuration: 300,
        showEasing: 'swing',
        hideMethod: 'fadeOut',
        hideDuration: 1000,
        hideEasing: 'swing',
        extendedTimeOut: 1000,
        positionClass: 'toast-top-center',
        timeOut: 3000,
        escapeHtml: false,
        newestOnTop: true,
        preventDuplicates: false,
        progressBar: true
        });
    @endif
    @if(\Session::has('user_create_success'))
    toastr.success('Success',"{{\Session::get('user_create_success')}}",{
    showMethod: 'fadeIn',
    showDuration: 300,
    showEasing: 'swing',
    hideMethod: 'fadeOut',
    hideDuration: 1000,
    hideEasing: 'swing',
    extendedTimeOut: 1000,
    positionClass: 'toast-top-center',
    timeOut: 3000,
    escapeHtml: false,
    newestOnTop: true,
    preventDuplicates: false,
    progressBar: true
    });
    @endif
  });
</script>
@endsection


