<div id="sidebar-wrapperss" class="bg-dark" style="height:100%;width:100%">
    <ul class="sidebar-nav nav navbar-inverse" style="">
        <div class="tabs">
            <div class="tab-button-outer">
                <ul id="tab-button">
                <li><a href="#tab01">Cameras</a></li>
                <li><a href="#tab02">Controller</a></li>
                </ul>
            </div>
            <div class="tab-select-outer">
                <select id="tab-select">
                <option value="#tab01">Cameras</option>
                <option value="#tab02">Controller</option>
                </select>
            </div>

            <div id="tab01" class="tab-contents">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search camera">
                <div class="input-group-append">
                <button class="btn btn-warning" type="button">
                    <i class="fa fa-search"></i>
                </button>
                </div>
            </div>
            <div id="html" class="demo mt-2">
                <ul>
                    <li data-jstree='{ "opened" : true }'>Lamppost
                        <ul>
                            <li data-jstree='{ "selected" : true }'>Camera 1</li>
                            <li>Camera 2</li>
                        </ul>
                    </li>
                </ul>
            </div>
            </div>
            <div id="tab02" class="tab-contents">
                <div class="row mt-2">
                    <div class="mx-auto">
                        <h5 class="text-center">Control Camera</h5>
                        <img src="{{asset('public/images/controlpal.png')}}" alt="Controller image" class="mb-2" usemap="#Map"><br>
                        <map name="Map">
                            <area shape="rect" coords="0,46,36,84" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_LEFT', false);" onmouseup="mouseUpPTZ('COMMAND_LEFT', false);">
                            <area shape="rect" coords="7,1,46,47" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_LEFT_UPPER', false);" onmouseup="mouseUpPTZ('COMMAND_LEFT_UPPER', false);">
                            <area shape="rect" coords="45,1,83,40" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_UPPER', false);" onmouseup="mouseUpPTZ('COMMAND_UPPER', false);">
                            <area shape="rect" coords="83,8,122,47" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_UPPER_RIGHT', false);" onmouseup="mouseUpPTZ('COMMAND_UPPER_RIGHT', false);">
                            <area shape="rect" coords="91,46,133,83" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_RIGHT', false);" onmouseup="mouseUpPTZ('COMMAND_RIGHT', false);">
                            <area shape="rect" coords="81,82,122,122" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_LOWER_RIGHT', false);" onmouseup="mouseUpPTZ('COMMAND_LOWER_RIGHT', false);">
                            <area shape="rect" coords="45,88,82,130" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_LOWER', false);" onmouseup="mouseUpPTZ('COMMAND_LOWER', false);">
                            <area shape="rect" coords="7,83,46,120" style="cursor:pointer" onmousedown="mouseDownPTZ('COMMAND_LEFT_LOWER', false);" onmouseup="mouseUpPTZ('COMMAND_LEFT_LOWER', false);">
                            <area shape="circle" coords="62,64,23" style="cursor:pointer" onclick="mouseDownPTZControl(9, true);">
                        </map>

                    </div>
                </div>
                <div class="row">
                    <div class="col-3 float-right">
                        <button class="btn btn-primary mb-2" onclick="lenplus()"> - </button>
                        <button class="btn btn-primary mb-2" onclick="focusplus()"> - </button>
                        <button class="btn btn-primary mb-2" onclick="apertureplus()"> - </button>
                    </div>
                    <div class="col-6">
                         <h5 class="mb-2 text-center" style="line-height: 2">Len</h5>
                         <h5 class="mb-2 text-center" style="line-height: 2">Focus</h5>
                         <h5 class="mb-2 text-center" style="line-height: 2">Aperture</h5>
                    </div>
                    <div class="col-3 float-left">
                        <button class="btn btn-primary mb-2" onclick="lenminus()">+</button>
                        <button class="btn btn-primary mb-2" onclick="focusminus()">+</button>
                        <button class="btn btn-primary mb-2" onclick="apertureminus()">+</button>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <h5 class="">Display : </h5>
                    </div>
                    <div class="col-2 mr-1">
                        <button class="btn btn-light btn-sm active" id="single">
                          <i class="fa fa-square"></i>
                        </button>
                    </div>
                    <div class="col-2 mr-1">
                        <button class="btn  btn-light btn-sm" id="double">
                          <i class="fa fa-th-large"></i>
                        </button>
                    </div>
                    <div class="col-2 mr-1">
                        <button class="btn  btn-light btn-sm" id="tripple">
                          <i class="fa fa-th"></i>
                        </button>
                    </div>
                    <div class="col-2 mr-1">
                        <button class="btn  btn-light btn-sm" id="fourth">
                          <i class="fa fa-th"></i>
                        </button>
                    </div>
                    <div class="col-2 mr-1">
                        <button class="btn  btn-light btn-sm" id="fifth">
                          <i class="fa fa-arrows-alt"></i>
                        </button>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                      <h5>3D ZOOM&nbsp;
                      <input type="checkbox" name="3d-zoom" id="3d-zoom" class="form-control-input" style="width:1.25rem;height:1.25rem;"></h5>
                    </div>
                </div>
            </div>

            </div>

        </ul>
    </div>
