<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('public/images/logo/p.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
           {{-- public/dist/img/AdminLTELogo.png --}}
      <span class="brand-text font-weight-light">PMEA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

         <li class="nav-item">
            <a href="{{url('/')}}" class="nav-link @if(Request::segment(1) == 'admin' && Request::segment(2) == 'dashboard') active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('lamppost')}}" class="nav-link @if(Request::segment(1) == 'admin' && Request::segment(2) == 'lamppost' && Request::segment(3) == '') active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Lamppost
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cube"></i>
              <p>
                Status
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview ">
              <li class="nav-item ">
                <a href="{{url('admin/lamppost/map')}}" class="nav-link @if(Request::segment(3) == 'map' && Request::segment(2) == 'lamppost') active @endif">
                  <i class="fas fa-map-marker nav-icon"></i>
                  <p>Map</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Manage
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('lamppost/cameras')}}" class="nav-link @if(Request::segment(2) == 'lamppost' && Request::segment(3) == 'cameras') active @endif">
                  <i class="fas fa-camera nav-icon"></i>
                  <p>Cameras</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/aqi')}}" class="nav-link @if(Request::segment(2) == 'lamppost' && Request::segment(3) == 'aqi') active @endif">
                  <i class="fa fa-water nav-icon"></i>
                  <p>AQI</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/lights')}}" class="nav-link @if(Request::segment(2) == 'lamppost' && Request::segment(3) == 'lights') active @endif">
                  <i class="fa fa-bolt nav-icon"></i>
                  <p>Lights</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/program')}}" class="nav-link @if(Request::segment(2) == 'lamppost' && Request::segment(3) == 'programs') active @endif">
                  <i class="fa fa-code nav-icon"></i>
                  <p>Programs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/displays')}}" class="nav-link">
                  <i class="fas fa-desktop nav-icon"></i>
                  <p>Displays</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/emergency_call')}}" class="nav-link @if(Request::segment(3) == 'emergency_call')active @endif">
                  <i class="fa fa-phone nav-icon"></i>
                  <p>Emergency Call</p>
                </a>
              </li>
              <li class="nav-item ">
                <a href="{{url('lamppost/emergency_info')}}" class="nav-link @if(Request::segment(3) == 'emergency_info')active @endif">
                  <i class="fa fa-info nav-icon"></i>
                  <p>Emergency Info</p>
                </a>
              </li>
            </ul>

          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-plus-circle"></i>
              <p>
                More
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('lamppost/group')}}" class="nav-link @if(Request::segment(3) == 'group')active @endif">
                  <i class="fa fa-object-group nav-icon"></i>
                  <p>Group</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/medias')}}" class="nav-link @if(Request::segment(3) == 'medias')active @endif">
                  <i class="fas fa-camera-retro nav-icon"></i>
                  <p>Medias</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('lamppost/settings')}}" class="nav-link @if(Request::segment(3) == 'settings')active @endif">
                  <i class="fa fa-cog nav-icon"></i>
                  <p>Settings</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{url('lamppost/log')}}" class="nav-link @if(Request::segment(2) == 'lamppost' && Request::segment(3) == 'logs') active @endif">
              <i class="nav-icon fas fa-sticky-note"></i>
              <p>
                Logs
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
