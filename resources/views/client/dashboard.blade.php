@extends('admin.include.layout')
@section('css')
<link rel="stylesheet" href="{{asset('public/css/map.css')}}">
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script> -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0fyIvyjrUNF6KPj_4VKi2aykLpAcDd-8&callback=initMap">
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.6.0/echarts-en.js"></script>
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
  </style>

<script>
    var map;
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
      });
    }
</script>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
      <div class="col-9 border-dark border-right mt-2">
        <div class="">
            <div id="map" style="height:640px;width:100%;">
            </div>
        </div>
      </div>
      <div class="col-3 mt-2">
        <h6>|Device Type -</h6>
        <div>
            <canvas id="pie"></canvas>
        </div>
        <h6>|Display Status</h6>
        <div class="status">
           <div class="row text-center">
               <div class="col-md-4 text-success">
                  <i class="fa fa-arrow-up"></i>
                  <p>0</p>
                  <p>ONLINE</p>
               </div>
               <div class="col-md-4 text-warning">
                  <i class="fa fa-arrow-down"></i>
                  <p>1</p>
                  <p>OFFLINE</p>
               </div>
               <div class="col-md-4 text-danger">
                  <i class="fa fa-times"></i>
                  <p>0</p>
                  <p>FAULT</p>
               </div>
           </div>
        </div>
        <h6>|Street Light Status</h6>
        <div class="status">
           <div class="row text-center">
               <div class="col-md-4 text-success">
                  <i class="fa fa-arrow-up"></i>
                  <p>0</p>
                  <p>ONLINE</p>
               </div>
               <div class="col-md-4 text-warning">
                  <i class="fa fa-arrow-down"></i>
                  <p>1</p>
                  <p>OFFLINE</p>
               </div>
               <div class="col-md-4 text-danger">
                  <i class="fa fa-times"></i>
                  <p>0</p>
                  <p>FAULT</p>
               </div>
           </div>
        </div>
        <h6>|Recent offline devices in last 7 days</h6>
        <div id="graph">

        <div id="container" style="width: 600px; height: 250px;"></div>
        </div>
      </div>
  </div>
</div>
@endsection
@section('script')
	<script>
		var ctx = document.getElementById("pie").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Display","Street Light"],
            datasets: [{
            backgroundColor: [
                "#C23531",
                "#141414"
            ],
            data: [12, 19]
            }]
        }
        });


        var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-33.863276, 151.207977),
          zoom: 12
        });
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
          downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}



      data = [["2000-06-05",116],["2000-06-06",129],["2000-06-07",135],["2000-06-08",86],["2000-06-09",73],["2000-06-10",85],["2000-06-11",73],["2000-06-12",68],["2000-06-13",92],["2000-06-14",130],["2000-06-15",245],["2000-06-16",139],["2000-06-17",115],["2000-06-18",111],["2000-06-19",309],["2000-06-20",206],["2000-06-21",137],["2000-06-22",128],["2000-06-23",85],["2000-06-24",94],["2000-06-25",71],["2000-06-26",106],["2000-06-27",84],["2000-06-28",93],["2000-06-29",85],["2000-06-30",73],["2000-07-01",83],["2000-07-02",125],["2000-07-03",107],["2000-07-04",82],["2000-07-05",44],["2000-07-06",72],["2000-07-07",106],["2000-07-08",107],["2000-07-09",66],["2000-07-10",91],["2000-07-11",92],["2000-07-12",113],["2000-07-13",107],["2000-07-14",131],["2000-07-15",111],["2000-07-16",64],["2000-07-17",69],["2000-07-18",88],["2000-07-19",77],["2000-07-20",83],["2000-07-21",111],["2000-07-22",57],["2000-07-23",55],["2000-07-24",60]];

var dateList = data.map(function (item) {
    return item[0];
});
var valueList = data.map(function (item) {
    return item[1];
});

option = {

    // Make gradient line here
    visualMap: [{
        show: false,
        type: 'continuous',
        seriesIndex: 0,
        min: 0,
        max: 400
    }, {
        show: false,
        type: 'continuous',
        seriesIndex: 1,
        dimension: 0,
        min: 0,
        max: dateList.length - 1
    }],


    title: [{
        left: 'center',
        text: 'Gradient along the y axis'
    }, {
        top: '55%',
        left: 'center',
        text: 'Gradient along the x axis'
    }],
    tooltip: {
        trigger: 'axis'
    },
    xAxis: [{
        data: dateList
    }, {
        data: dateList,
        gridIndex: 1
    }],
    yAxis: [{
        splitLine: {show: false}
    }, {
        splitLine: {show: false},
        gridIndex: 1
    }],
    grid: [{
        bottom: '60%'
    }, {
        top: '60%'
    }],
    series: [{
        type: 'line',
        showSymbol: false,
        data: valueList
    }, {
        type: 'line',
        showSymbol: false,
        data: valueList,
        xAxisIndex: 1,
        yAxisIndex: 1
    }]
};

var chart = document.getElementById('graph');
    		var myChart = echarts.init(chart);
    </script>

@endsection
