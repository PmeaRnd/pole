<?php

namespace App\Http\Controllers\Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DebugLog extends Controller
{
    public function index(Request $request)
    {
        return response()->json(['success' => true, 'code' => 200, 'streaming_url' => 'rtsp://admin:admin@123@192.168.1.108/cam/realmonitor?channel=1&subtype=1']);
    //    return view('admin.lamppost.camera.index');
    }
}
