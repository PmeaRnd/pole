<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use DataTables;
use App\Models\Program as UserProgram;
use App\Models\Log as LogActivity;
use Log;

class Program extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()){
            $log = [];
            $log['subject'] = 'View all program';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

            LogActivity::create($log);
        }
        if ($request->ajax()) {
            $data = UserProgram::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="'.url("admin/lamppost/programs").'/'.$row->id.'/edit" class="btn  btn-info btn-sm"><i class="fa fa-info"></i></a> <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="view-btn" data-id="'.$row->id.'"><i class="fa fa-eye"></i></a>&nbsp;<button class="btn  btn-dark btn-sm" id="send-btn" data-id="'.$row->id.'"><i class="fa fa-paper-plane"></i></button>&nbsp;';

                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button>&nbsp;';

                           $btn .= '<a href="'.url("admin/user").'/'.$row->id.'/edit" class="btn btn-warning btn-sm" id="query-btn" data-id="'.$row->id.'"><i class="fa fa-question"></i></a></div>';
                            return $btn;
                    })
                    ->addColumn('renew_times', function($row){
                        return date('d-m-Y H:i:s',strtotime($row->renew_time));
                    })
                    ->rawColumns(['action','renew_times'])
                    ->make(true);
        }
        return view('admin.lamppost.programs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Create new program.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        return response()->json(['success' => true, 'code' => 202,'message' => 'create new']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Retrive program info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        // $media = UserProgram::find($id);
        $media = null;
        // if($media){
            return response()->json(['success' => true, 'code' => 202,'type' => 'video','media_link' => 'micky.mp4']);
        // }
        // else{
        //     return response()->json(['success' => false, 'code' => 404,'type' => null,'media_link' => null]);
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete program.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        // UserProgram::where('id','=',$id)->delete();
        return response()->json(['success' => true, 'code' => 202,'message' => 'Program delete successfully.']);
    }

    public function send(Request $request,$id)
    {
        $log = [];
    	$log['subject'] = 'Execute program.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

       $table = UserProgram::find($id);
        // if($table){
            // return response()->json(['success' => true, 'code' => 202,'data' => $table]);
        // }
        // else{
        //    return response()->json(['success' => true, 'code' => 202,'data' => $table]);
        // }

        return response()->json(['success' => true, 'code' => 202]);
    }

    public function query(Request $request,$id)
    {

    }

    public function info(Request $request,$id)
    {
        $log = [];
    	$log['subject'] = 'Program info fetch.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);


        return response()->json(['success' => true, 'code' => 202,'message' => 'create new']);
    }


    public function make(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Make new program.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        // dd($request->all());
        return view('admin.lamppost.program.new');
    }
}
