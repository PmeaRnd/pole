<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use App\Models\Pole;

class Lamppost extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Lamppad Status
    public function home(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost Home view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.lamppost.home');
    }

    public function map(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost map view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.lamppost.map');
    }

    public function cameras(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost Camera view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $poles = Pole::all();
        return view('admin.lamppost.cameras',compact('poles'));
    }

    public function aqi(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost aqi view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.lamppost.aqi');
    }

    public function lights(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost Light view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.lamppost.lights');
    }

    public function programs(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Lamppost Programs view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.lamppost.programs');
    }
}
