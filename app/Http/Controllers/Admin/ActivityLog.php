<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use Carbon\Carbon;
use Validator;
use DataTables;
use Auth;

class ActivityLog extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = LogActivity::latest()->get();
            // dd($data);
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('url_method', function($row){
                        $method = '';
                        if($row->method == 'GET'){
                           $method .= '<div class="text-center"><span class="badge badge-primary">'.$row->method.'</span></div>';
                        }
                        else if($row->method == 'DELETE'){
                            $method .= '<div class="text-center"><span class="badge badge-danger">'.$row->method.'</span></div>';
                         }
                         else if($row->method == 'POST'){
                            $method .= '<div class="text-center"><span class="badge badge-success">'.$row->method.'</span></div>';
                         }
                         else{
                            $method .= '<div class="text-center"><span class="badge badge-warning">'.$row->method.'</span></div>';
                         }
                         return $method;
                    })
                    ->addColumn('subjects', function($row){
                        return '<p class="text-center text-primary font-weight-bold font-weight-italic">'.$row->subject.'</p>';
                    })
                    ->addColumn('new_time', function($row){
                        return '<div class="text-center text-secondary">'.date('d F Y h:i a',strtotime($row->created_at)).'</div>';
                    })
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="javascript:void(0)" class="btn btn-primary btn-sm" id="view-btn" data-id="'.$row->id.'"><i class="fa fa-eye"></i></a>&nbsp;';

                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button>&nbsp;';
                            return $btn;
                    })
                    ->addColumn('agents', function($row){
                        return '<div class="text-center"><i class="text-primary font-weight-bold">'.substr($row->agent,0,12).'...</i></div>';
                    })
                    ->rawColumns(['action','url_method','subjects','new_time','agents'])
                    ->make(true);
        }
        return view('log.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $log = [];
    	// $log['subject'] = 'Store media file';
    	// $log['url'] = $request->fullUrl();
    	// $log['method'] = $request->method();
    	// $log['ip'] = $request->ip();
    	// $log['agent'] = $request->header('user-agent');
        // $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        // LogActivity::create($log);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Media preview';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $log = LogActivity::select('*')->where('id','=',$id)->first();
        return response()->json(['success' => true, 'code' => 200, 'message' => 'Log info fetch successfully.', 'data' => $log]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $log = [];
    	// $log['subject'] = 'Update media information';
    	// $log['url'] = $request->fullUrl();
    	// $log['method'] = $request->method();
    	// $log['ip'] = $request->ip();
    	// $log['agent'] = $request->header('user-agent');
        // $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        // LogActivity::create($log);

        // DB::table('media')->where('id','=',$id)->update(['name' => $request->media_name, 'tag' => $request->media_tag, 'updated_at' => Carbon::now()]);

        // return response()->json(['success' => true,'code' => 200 , 'message' => 'Media info updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete Log file.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        LogActivity::where('id' , '=', $id)->delete();
        return response()->json(['success' => true, 'code' =>202, 'message' => 'Log deleted successfully.']);
    }

}
