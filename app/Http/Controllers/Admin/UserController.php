<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use Carbon\Carbon;
use Validator;
use DataTables;
use App\User;
use Log;
use App\Models\Pole;
use App\Models\PoleAccess;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()){
            $log = [];
            $log['subject'] = 'View all users for lamppost.';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

            LogActivity::create($log);
        }
        if ($request->ajax()) {
            $data = User::where('is_admin',false)->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="javascript:void(0)" class="btn btn-primary btn-sm" id="view-btn" data-id="'.$row->id.'"><i class="fa fa-eye"></i></a>&nbsp;';
                           $btn .= '<a href="'.url("admin/user").'/'.$row->id.'/edit" class="btn btn-success btn-sm" id="edit-btn" data-id="'.$row->id.'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button></div>';
                            return $btn;
                    })
                    ->addColumn('pole_links', function($row){
                        $accesses = PoleAccess::where('user_id',$row->id)->get();
                        $poles = '<div class="text-center">';
                        if($accesses->count() > 0){
                            foreach($accesses as $access){
                              $poles.= '<span class="badge badge-primary">'.$access->pole['name'].'</span>&nbsp;';
                            }
                        }
                        else{
                            $poles.= '<span class="badge badge-dark">No Pole</span>';
                        }
                        $poles .= "</div>";
                        return $poles;
                    })
                    ->rawColumns(['action','pole_links'])
                    ->make(true);
        }
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Create new user.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        $poles = Pole::all();
        return view('admin.user.create',compact('poles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Save new user.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required_with:password|same:password|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $user  = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->is_admin = false;
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            $user->save();

            if($request->has('poles') && count($request->poles) > 0){
                foreach($request->poles as $pole){
                    $pol = new PoleAccess;
                    $pol->user_id = $user->id;
                    $pol->pole_id = $pole;
                    $pol->created_at = Carbon::now();
                    $pol->updated_at = Carbon::now();
                    $pol->save();
                }
            }


            return redirect('admin/user')->with(['user_create_success' => 'User created successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Retrieve user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

       $result = User::where('id','=',$id)->first();
       if($result){
        return response()->json(['success' => true,'code' => 202, 'message' => 'Delete user successfully.','data' => $result]);
       }
       else{
        return response()->json(['success' => false,'code' => 501, 'message' => 'Delete user failed.','data' => null]);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Edit user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        $user = User::find($id);
        if($user){
            $accesses = PoleAccess::where('user_id',$id)->get();
            $poles = Pole::all();
            $polesaccess = [];
            if($accesses->count() > 0){
                foreach($accesses as $access){
                    array_push($polesaccess,$access->pole['identity']);
                }
            }
            return view('admin.user.edit',compact('user','poles','polesaccess'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = [];
    	$log['subject'] = 'Update user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required_with:password|same:password|min:6',
            'poles' => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $user  = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->updated_at = Carbon::now();
            $user->save();

            PoleAccess::where('user_id',$id)->delete();

            if($request->has('poles') && count($request->poles) > 0){
                foreach($request->poles as $pole){
                    $pol = new PoleAccess;
                    $pol->user_id = $id;
                    $pol->pole_id = $pole;
                    $pol->created_at = Carbon::now();
                    $pol->updated_at = Carbon::now();
                    $pol->save();
                }
            }
            return redirect('admin/user')->with(['user_edit_success' => 'User updated successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

       $result = User::where('id','=',$id)->delete();
       PoleAccess::where('user_id',$id)->delete();
       if($result){
        return response()->json(['success' => true,'code' => 202, 'message' => 'Delete user successfully.']);
       }
       else{
        return response()->json(['success' => false,'code' => 501, 'message' => 'Delete user failed.']);
       }
    }
}
