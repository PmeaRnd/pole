<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use DataTables;
use App\Models\Media as Multimedia;
use App\Models\Log as LogActivity;
use Log;
use DB,Image;

class Media extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()){
            $log = [];
            $log['subject'] = 'User media view';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

            LogActivity::create($log);
        }
        if ($request->ajax()) {
            $data = Multimedia::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="javascript:void(0)" class="btn btn-dark btn-sm" id="edit-btn" data-id="'.$row->id.'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

                           $btn .= '<button class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button>&nbsp;';
                            return $btn;
                    })
                    ->addColumn('snap',function($row){
                          $image = '<img src="'.asset('public/images/bg').'/'.$row->preview.'" width="50px" height="50px" class="mx-auto" id="media_img" data-id="'.$row->id.'">';
                          return $image;
                    })
                    ->addColumn('media',function($row){
                        $checkbox = '<input type="checkbox" name="media" id="media" value="'.$row->id.'">';
                        return $checkbox;
                  })
                    ->rawColumns(['action','snap','media'])
                    ->make(true);
        }

        return view('admin.lamppost.media.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Store media file';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        Log::info($request->all());
        if ( ! $request->has('file') ) {
            echo 'Error: ' . 'occured' . '<br>';
            return response()->json(['success' => false, 'code' => 501, 'message' => 'Media upload failed.']);
        }
        else {
            $uploadedFile = $request->file('file');
            $filename = time().$uploadedFile->getClientOriginalName();
            $request->file->move(public_path('/uploads'), $filename);

            $img = Image::make($uploadedFile)->resize(320, 240)->insert(public_path("media/thumbnail/thumb_$filename"));

            $media_type = explode("/",$uploadedFile->getMimeType());

            $media = new Multimedia;
            $media->preview = 'thumb_'.$filename;
            $media->name = $filename;
            $media->type = $uploadedFile->getMimeType();
            $media->tag = $media_type[0];
            $media->uploaded_by = Auth::user()->name;
            $media->created_at = Carbon::now();
            $media->updated_at = Carbon::now();
            $media->save();

            return response()->json(['success' => true, 'code' => 202, 'message' => 'Media upload successfull.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $log = [];
    	$log['subject'] = 'Media preview';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $media = Multimedia::select('id','name','tag')->where('id','=',$id)->first();
        return response()->json(['success' => true, 'code' => 200, 'message' => 'Media info fetch successfully.', 'data' => $media]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = [];
    	$log['subject'] = 'Update media information';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        DB::table('media')->where('id','=',$id)->update(['name' => $request->media_name, 'tag' => $request->media_tag, 'updated_at' => Carbon::now()]);

        return response()->json(['success' => true,'code' => 200 , 'message' => 'Media info updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete media file.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        Multimedia::where('id' , '=', $id)->delete();
        return response()->json(['success' => true, 'code' =>202, 'message' => 'Media deleted successfully.']);
    }

    public function media(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Retrieve media file.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $file = Multimedia::select('media')->where('id','=', $request->id)->first();
        Log::info($file);
        return response()->json(['success' => true, 'code' => 200, 'message' => 'Media file fetch successfully.','data' => $file]);

    }
}
