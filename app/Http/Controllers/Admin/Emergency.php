<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use DataTables;
use App\Models\Info;
use App\Models\Log as LogActivity;
use Log;

class Emergency extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete emergency info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
        LogActivity::create($log);

        Info::where('id','=',$id)->delete();
        return response()->json(['success' => true, 'code' => 202, 'message' => 'Program deleted successfully']);
    }

    public function call(Request $request)
    {
        $log = [];
    	$log['subject'] = 'View emergency call info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
        LogActivity::create($log);
         return view('admin.lamppost.emergency.call');
    }

    public function info(Request $request)
    {
        if ($request->ajax()) {
            $data = Info::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="'.url("admin/lamppost/programs").'/'.$row->id.'/info" class="btn  btn-primary btn-sm"><i class="fa fa-eye"></i></a>&nbsp';

                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('admin.lamppost.emergency.info');
    }
}
