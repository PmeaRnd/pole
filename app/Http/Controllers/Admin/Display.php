<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use App\Models\DisplayUpload;
use Carbon\Carbon;
use Storage,Auth;

class Display extends Controller
{
    public function index(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Display properties view';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
        LogActivity::create($log);
        return view('admin.lamppost.display.index');
    }

    public function buffer(Request $request)
    {
        if($request->ajax()){
            $uploadedFile = $request->file('videoUpload');
            $filename = time().$uploadedFile->getClientOriginalName();
            $request->videoUpload->move(public_path('/uploads/core'), $filename);
            $data = ['url'=> url('public/uploads/core')."/".$filename];
            return response()->json(['success' => true, 'code' => 200, 'message' => 'Video upload successfully.','data' => $data]);
        }
        else{

            return "Invalid request";
        }
    }

    public function upload(Request $request)
    {
       $filename = null;
       $media_text = null;
       $props = [];
       if($request->fileType == 'Text'){
         $media_text = $request->uploadText;
         $props = [
             'align' => $request->align ? $request->align : 'left',
             'bold' => $request->bold ? true : false,
             'italic' =>  $request->italic ? true : false,
             'font-size' => $request->size ? $request->size : 18,
             'font-color' => $request->color ? $request->color : '#ffffff'
         ];
       }
       else if($request->fileType == 'Image'){
        $uploadedFile = $request->file('imgUpload');
        $filename = time().$uploadedFile->getClientOriginalName();
        $request->imgUpload->move(public_path('/uploads/core'), $filename);
       }
       else{
        $uploadedFile = $request->file('videoUpload');
        $filename = time().$uploadedFile->getClientOriginalName();
        $request->videoUpload->move(public_path('/uploads/core'), $filename);
       }


       $upload = new DisplayUpload;
       $upload->lampost_id = 2;
       $upload->user_id = Auth::id();
       $upload->media_type = $request->fileType;
       $upload->filename = $filename;
       $upload->media_text = $media_text;
       $upload->media_prop = json_encode($props);
       $upload->created_at = Carbon::now();
       $upload->updated_at = Carbon::now();
       $upload->save();

       $ip = $request->ip();
       if($ip == '::1'){
           $ip = '127.0.0.1';
       }
       $log = [];
       $log['subject'] = 'Send data to LED Display.';
       $log['url'] = $request->fullUrl();
       $log['method'] = $request->method();
       $log['ip'] = $ip;
       $log['agent'] = $request->header('user-agent');
       $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
        LogActivity::create($log);

       return response()->json(['success' => true, 'code' => 200, 'error' => '{}','message' => 'Data send to display successfully.'],200);

    }
}
