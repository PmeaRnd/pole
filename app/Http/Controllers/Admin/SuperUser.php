<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Log as LogActivity;
use Carbon\Carbon;
use Validator;
use DataTables;
use App\User;
use Log;

class SuperUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()){
            $log = [];
            $log['subject'] = 'View all admin list for lamppost.';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

            LogActivity::create($log);
        }
        if ($request->ajax()) {
            $data = User::where('is_admin',true)->latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="javascript:void(0)" class="btn btn-primary btn-sm" id="view-btn" data-id="'.$row->id.'"><i class="fa fa-eye"></i></a>&nbsp;';
                           $btn .= '<a href="'.url("admin/superuser").'/'.$row->id.'/edit" class="btn btn-success btn-sm" id="edit-btn" data-id="'.$row->id.'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button></div>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.superuser.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Create new user.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        return view('admin.superuser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $log = [];
    	$log['subject'] = 'Save new super user.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required_with:password|same:password|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $user  = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->is_admin = true;
            $user->created_at = Carbon::now();
            $user->updated_at = Carbon::now();
            $user->save();

            return redirect('admin/user')->with(['user_create_success' => 'User created successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Retrieve super user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

       $result = User::where('id','=',$id)->where('is_admin',true)->first();
       if($result){
        return response()->json(['success' => true,'code' => 202, 'message' => 'Delete user successfully.','data' => $result]);
       }
       else{
        return response()->json(['success' => false,'code' => 501, 'message' => 'Delete user failed.','data' => null]);
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Edit super user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);
        $user = User::find($id);
        if($user){
            return view('admin.superuser.edit',compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = [];
    	$log['subject'] = 'Update super user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required_with:password|same:password|min:6',
            'poles' => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $user  = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->is_admin = true;
            $user->password = $request->password;
            $user->updated_at = Carbon::now();
            $user->save();

            return redirect('admin/user')->with(['user_edit_success' => 'User updated successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $log = [];
    	$log['subject'] = 'Delete super user info.';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';

        LogActivity::create($log);

       $result = User::where('id','=',$id)->delete();

       if($result){
        return response()->json(['success' => true,'code' => 202, 'message' => 'Delete user successfully.']);
       }
       else{
        return response()->json(['success' => false,'code' => 501, 'message' => 'Delete user failed.']);
       }
    }
}
