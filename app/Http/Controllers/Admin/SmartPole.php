<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use App\Models\PoleAccess;
use App\User;
use App\Models\Pole;
use Carbon\Carbon;
use Validator;
use DataTables;
use DB;
use Log;
use App\Models\Camera;
use App\Models\Screen;
use App\Models\Sensor;


class SmartPole extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->ajax()){
            $log = [];
            $log['subject'] = 'View all linked lamppost.';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
            LogActivity::create($log);
        }
        if ($request->ajax()) {
            $data = Pole::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<div class="text-center"><a href="javascript:void(0)" class="btn btn-primary btn-sm" id="view-btn" data-id="'.$row->id.'"><i class="fa fa-eye"></i></a>&nbsp;';
                           $btn .= '<a href="'.url("admin/pole").'/'.$row->id.'/edit" class="btn btn-success btn-sm" id="edit-btn" data-id="'.$row->id.'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
                           $btn .= '<button  class="btn btn-danger btn-sm" id="delete-btn" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button></div>';
                            return $btn;
                    })
                    ->addColumn('locations', function($row){
                        return '<div class="text-center"><span class="badge badge-warning">'.$row->location.'</span></div>';
                    })
                    ->addColumn('ips', function($row){
                        return '<div class="text-center"><span class="badge badge-success">'.$row->ip.'</span></div>';
                    })
                    ->addColumn('names', function($row){
                        return '<div class="text-center"><span class="badge badge-secondary">'.$row->name.'</span></div>';
                    })
                    ->addColumn('link_users', function($row){
                        $accesses =PoleAccess::where('pole_id','=',$row->identity)->get();

                        $users = "<div class='text-center'>";
                        if($accesses->count() > 0)
                        {
                            foreach($accesses as $access)
                            {
                               $users .= '<span class="badge badge-primary">'.$access->user['name'].'</span>&nbsp;';
                            }
                        }
                        else
                        {
                            $users = '<div class="text-center"><span class="badge badge-dark">No Users</span></div>';
                        }

                        $users .= "</div>";

                        return $users;
                    })
                    ->rawColumns(['link_users','action','locations','ips','names'])
                    ->make(true);
        }
        return view('admin.pole.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('admin.pole.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
            'ip' => 'required|ip',
            'location' => 'nullable|min:3',
            'users' => 'nullable',
            'ptz_ip' => 'required|ip',
            'ptz_username' => 'required',
            'ptz_password' => 'required',
            'sensor_ip' => 'required|ip',
            'sensor_username' => 'required',
            'sensor_password' => 'required',
            'screen_ip' => 'required|ip',
            'screen_username' => 'required',
            'screen_password' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/pole/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $str=rand();
            $result = md5($str);

            $pole = new Pole;
            $pole->name = $request->name;
            $pole->identity = $result;
            $pole->ip = $request->ip;
            $pole->location = $request->location;
            $pole->created_at = Carbon::now();
            $pole->updated_at = Carbon::now();
            $pole->save();

            $id = $result;

            if($request->has('users') && count($request->users) > 0){
                foreach($request->users as $user)
                {
                    $poleaccess = new PoleAccess;
                    $poleaccess->user_id = $user;
                    $poleaccess->pole_id = $id;
                    $poleaccess->created_at = Carbon::now();
                    $poleaccess->updated_at = Carbon::now();
                    $poleaccess->save();
                }
            }

            $pole_id = $pole->id;

            $camera = new Camera;
            $camera->pole_id = $pole_id;
            $camera->ip = $request->ptz_ip;
            $camera->username = $request->ptz_username;
            $camera->password = $request->ptz_password;
            $camera->created_at = Carbon::now();
            $camera->updated_at = Carbon::now();
            $camera->save();

            $screen = new Screen;
            $screen->pole_id = $pole_id;
            $screen->ip = $request->screen_ip;
            $screen->username = $request->screen_username;
            $screen->password = $request->screen_password;
            $screen->created_at = Carbon::now();
            $screen->updated_at = Carbon::now();
            $screen->save();

            $sensor = new Sensor;
            $sensor->pole_id = $pole_id;
            $sensor->ip = $request->sensor_ip;
            $sensor->username = $request->sensor_username;
            $sensor->password = $request->sensor_password;
            $sensor->created_at = Carbon::now();
            $sensor->updated_at = Carbon::now();
            $sensor->save();


            return redirect('admin/pole')->with(['pole_create' => 'New Smart Pole Added Successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pole = Pole::find($id);
        if($pole){
            $accesses = PoleAccess::where('pole_id',$pole->identity)->get();
            $users = [];
            if($accesses->count() > 0){
              foreach($accesses as $access){
                  array_push($users, $access->user['name']);
              }
            }
            $data = ['pole' => $pole,'users' => $users];
            return response()->json(['success' => true, 'code' => 200, 'data' => $data, 'message' => 'Pole info access successfully']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pole = Pole::find($id);
        if($pole){
            $users = User::all();
            $accesses = PoleAccess::where('pole_id','=',$pole->identity)->get();
            $selectuser = [];
            if($accesses->count() > 0){
                foreach($accesses as $access){
                   array_push($selectuser,$access->user['id']);
                }
            }
            return view('admin.pole.edit',compact('pole','selectuser','users'));
        }
        else{
            return response()->view('errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
            'ip' => 'required|ip',
            'location' => 'nullable|min:3',
            'users' => 'nullable'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $str=rand();
            $result = md5($str);

            DB::table('poles')
            ->where('identity', $id)
            ->update([
                'name' => $request->name,
                'ip' => $request->ip,
                'location' => $request->location,
                'updated_at' => Carbon::now()
            ]);

            PoleAccess::where('pole_id','=',$id)->delete();

            if($request->has('users') && count($request->users) > 0){
                foreach($request->users as $user)
                {
                    $poleaccess = new PoleAccess;
                    $poleaccess->user_id = $user;
                    $poleaccess->pole_id = $id;
                    $poleaccess->created_at = Carbon::now();
                    $poleaccess->updated_at = Carbon::now();
                    $poleaccess->save();
                }
            }

            $id = Pole::where('identity',$id)->first()->id;
            DB::table('cameras')
            ->where('pole_id', $id)
            ->update([
                'username' => $request->ptz_username,
                'ip' => $request->ptz_ip,
                'password' => $request->ptz_password,
                'updated_at' => Carbon::now()
            ]);

            DB::table('sensors')
            ->where('pole_id', $id)
            ->update([
                'username' => $request->sensor_username,
                'ip' => $request->sensor_ip,
                'password' => $request->sensor_password,
                'updated_at' => Carbon::now()
            ]);

            DB::table('screens')
            ->where('pole_id', $id)
            ->update([
                'username' => $request->screen_username,
                'ip' => $request->screen_ip,
                'password' => $request->screen_password,
                'updated_at' => Carbon::now()
            ]);

            return redirect('admin/pole')->with(['pole_edit' => 'Smart Pole Edited Successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pole = Pole::find($id);
        $identity = $pole->identity;
        Pole::where('id',$id)->delete();
        PoleAccess::where('pole_id',$identity)->delete();
        return response()->json(['success' => true, 'code' => 200, 'message' => 'Smart Pole Deleted Successfully']);
    }


    public function userselected(Request $request,$id)
    {
      $identity = $request->id;
      $accesses = PoleAccess::where('pole_id','=',$identity)->get();

      $data = [];
      if($accesses->count() > 0){
          foreach($accesses as $access){
              $array = ['id' => $access->user['id'], 'text' => $access->user['name']];
              array_push($data,$array);
          }
      }

      return response()->json($data);
    }
}
