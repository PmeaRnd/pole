<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use Auth;

class Dashboard extends Controller
{
    public function __construct(Request $request)
    {

    }

    public function index(Request $request)
    {
        $log = [];
    	$log['subject'] = 'View Dashboard';
    	$log['url'] = $request->fullUrl();
    	$log['method'] = $request->method();
    	$log['ip'] = $request->ip();
    	$log['agent'] = $request->header('user-agent');
        $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
        LogActivity::create($log);
        return view('admin.dashboard.index');
    }
}
