<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pole;
use Log,Storage;
use App\Models\Camera as PTZ;


class Camera extends Controller
{
    // Load all camera list connected with all lamppost
    public function camera_list(Request $request)
    {
        $poles = Pole::query();
        if($request->has('keyword') && $request->keyword != null){
            $poles = $poles->where('name', 'like', '%' . "$request->keyword" . '%')->get();
        }
        else{
            $poles = $poles->get();
        }
        $result = [];
        if($poles->count() > 0){
            foreach($poles as $pole){
                $data = [
                    'ip' => $pole->ip,
                    'name' => $pole->name,
                    'location' => $pole->location,
                    'camera_ip' => $pole->camera['ip'],
                    'camera_username' => $pole->camera['username'],
                    'camera_password' => $pole->camera['password']];
                array_push($result,$data);
            }
            return response()->json(['success' => true, 'code' => 400, 'data' => $result, 'message' => 'load camera list successful.'],200);
        }
        else{
            return response()->json(['success' => true, 'code' => 400, 'data' => [], 'message' => 'load camera list successful.'],200);
        }

    }

    // Stream all link related to the the lamppost.
    public function stream(Request $request)
    {
       $stream_url = [];
       if($request->has('camera_ips') && count($request->camera_ips)> 0){
         // loop throught all the camera list ip to call shell script and also retrieve stream link.
         foreach($request->camera_ips as $camera){
            $data = PTZ::where('ip',$camera)->first();
            $name = str_replace('.','',$camera);
            // extension for shell and codec stript
            $codec = $name.'.m3u8';
            $shell = $name.'.sh';
            // create shell screept code
            $shell_data = "";
            $video_data = "rtsp://$data->username:$data->password@$data->ip:554/cam/realmonitor?channel=1&subtype=1";
            $video_data = '"'.$video_data.'"';
            $shell_data .= "VIDSOURCE=$video_data"."\r\n";
            $shell_data .= 'AUDIO_OPTS="-c:a aac -b:a 160000 -ac 2 "'."\r\n";
            $shell_data .= 'VIDEO_OPTS="-s 854x480 -c:v libx264 -b:v 800000"'."\r\n";
            $shell_data .= 'OUTPUT_HLS="-hls_time 10 -hls_list_size 10 -start_number 1"'."\r\n";
            $shell_data .= 'ffmpeg -i "$VIDSOURCE" -y $AUDIO_OPTS $VIDEO_OPTS $OUTPUT_HLS '."$codec";
            // create local file for shell script and also codec for view view stream
            Storage::disk('local')->put("stream/$codec", '');
            Storage::disk('local')->put("stream/$shell", $shell_data);
            // call shell script
            $shell = "sh ".storage_path("app/stream/$shell")."> /dev/null 2>&1 & echo $!";
            // $pid = exec($shell);
            array_push($stream_url,storage_path("app/stream/$codec"));
         }
         // If perticular camera selected from list.
         return response()->json(['success' => true, 'code' => 200, 'data' => $stream_url, 'message' => 'stream url fetch successfully']);
       }
       else{
         // retrieve and call the camera by default.
        return response()->json(['success' => true, 'code' => 200, 'data' => [], 'message' => 'stream url fetch successfully']);
       }
    }

    public function store(Request $request)
    {
        $camera = '192.168.1.108';
        $data = PTZ::where('ip',$camera)->first();
        $name = str_replace('.','',$camera);
        $codec = $name.'.m3u8';
        $shell = $name.'.sh';

        $shell_data = "";
        $video_data = "rtsp://$data->username:$data->password@$data->ip:554/cam/realmonitor?channel=1&subtype=1";
        $video_data = '"'.$video_data.'"';
        $shell_data .= "VIDSOURCE=$video_data"."\r\n";
        $shell_data .= 'AUDIO_OPTS="-c:a aac -b:a 160000 -ac 2 "'."\r\n";
        $shell_data .= 'VIDEO_OPTS="-s 854x480 -c:v libx264 -b:v 800000"'."\r\n";
        $shell_data .= 'OUTPUT_HLS="-hls_time 10 -hls_list_size 10 -start_number 1"'."\r\n";
        $shell_data .= 'ffmpeg -i "$VIDSOURCE" -y $AUDIO_OPTS $VIDEO_OPTS $OUTPUT_HLS '."$codec";
        Storage::disk('local')->put("stream/$codec", '');
        Storage::disk('local')->put("stream/$shell", $shell_data);

        shell_exec(storage_path("app/stream/$shell"));
        // Log::info($output);

        return view('test');
    }
}
