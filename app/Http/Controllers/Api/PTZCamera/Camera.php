<?php

namespace App\Http\Controllers\Api\PTZCamera;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Guzzle\Http\Client;
use Exception;
use Validator;

class Camera extends Controller
{
    public $ip ,$user, $pass;
    public function __construct()
    {
       $this->ip = "192.168.1.108";
       $this->user = "admin";
       $this->pass = "admin@123";
    }
    public function monitor(Request $request)
    {
       return response()->json(['success' => true, 'code' => 200, 'camera_url' => 'rtsp://IPADDRESS:1554//cam/realmonitor'],200);
    }

    public function streaming(Request $request)
    {
        $stream_url  = "rtsp://admin:admin@123@$this->ip/cam/realmonitor?channel=1&subtype=1";
        return response()->json(['success' => true, 'code' => 202, 'streaming_url' => $stream_url]);
    }

    public function control(Request $request)
    {
        // $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'ip' => 'required|ip',
            'action' => 'required|in:start,stop',
            'channel' => 'required|in:0,1',
            'code' => 'required|in:Up,Down,Left,Right,ZoomWide,ZoomTele,FocusNear,FocusFar,IrisLarge,IrisSmall,GotoPreset,SetPreset,ClearPreset,StartTour,StopTour,LeftUp,RightUp,LeftDown,RightDown,AddTour,DelTour,ClearTour,AutoPanOn,AutoPanOff,SetLeftLimit,SetRightLimit,AutoScanOn,AutoScanOff,SetPatternBegin,SetPatternEnd,StartPattern,StopPattern,ClearPattern,Position,AuxOn,AuxOff,Menu,Exit,Enter,MenuUp,MenuDown,MenuLeft,MenuRight,Reset,LightController,PositionABS,Continuously',
            'arg1' => 'required|in:0,1',
            'arg2' => 'required|min:1|max:5',
            'arg3' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'code' => 422,'message' => null, 'error' => $validator->errors()], 422);
        }
        else{
            $uri = "http://$request->ip/cgi-bin/ptz.cgi";
            $username = "admin";
            $password = "admin@123";

            $array = [
               'action' => $request->action,
               'channel' => $request->channel,
               'code' => $request->code,
               'arg1' => $request->arg1,
               'arg2' => $request->arg2,
               'arg3' => $request->arg3
            ];

            cameraAPI($uri,$username,$password,$array);
            return response()->json(['success' => true, 'code' => 202,'message' => 'Camera control successfully.', 'error' => null], 202);
        }



        // $options = array(
        //         CURLOPT_URL            => $url,
        //         CURLOPT_HEADER         => true,
        //         CURLOPT_VERBOSE        => true,
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_FOLLOWLOCATION => true,
        //         CURLOPT_SSL_VERIFYPEER => false,    // for https
        //         CURLOPT_USERPWD        => $username . ":" . $password,
        //         CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
        //         CURLOPT_POST           => true,
        //         // CURLOPT_POSTFIELDS     => http_build_query($post_data)
        // );

        // $ch = curl_init();

        // curl_setopt_array( $ch, $options );

        // try {
        //   $raw_response  = curl_exec( $ch );

        //   // validate CURL status
        //   if(curl_errno($ch))
        //       throw new Exception(curl_error($ch), 500);

        //   // validate HTTP status code (user/password credential issues)
        //   $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //   if ($status_code != 200)
        //       throw new Exception("Response with Status Code [" . $status_code . "].", 500);

        // } catch(Exception $ex) {
        //     if ($ch != null) curl_close($ch);
        //     throw new Exception($ex);
        // }

        // if ($ch != null) curl_close($ch);

        // // End clock time in seconds
        // $end_time = microtime(true);
        // // echo "raw response: " . $raw_response;
        // $execution_time = ($end_time - $start_time);
        // echo " Execution time of script = ".$execution_time." sec";
    }

    public function snapshot(Request $request)
    {
         $uri = "http://$request->ip/cgi-bin/snapshot.cgi";

         $array = [
            'channel'=> 1
         ];
         $data = cameraAPI($uri,'admin','admin@123',$array);
         print_r($data);
         return response()->json(['success' => true, 'code' => 202,'message' => 'Take Snapshot successfully.', 'error' => null], 202);
    }

}
