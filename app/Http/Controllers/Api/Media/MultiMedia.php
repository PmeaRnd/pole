<?php

namespace App\Http\Controllers\Api\Media;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Media;
use Carbon\Carbon;
use Validator;

class MultiMedia extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('media');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media_file' => 'required|mimes:mpga,wav,jpeg,png,jpg,gif,svg,mpeg,mp4,avi,3gp,ogg|max:10240'
        ]);

        if($validator->fails()){
            return response()->json(['success' => false, 'code' => 422,'message' => null, 'error' => $validator->errors()], 422);
        }
        else{
            $types = '';
            $uploadedFile = $request->file('media_file');
            $filename = time().$uploadedFile->getClientOriginalName();

            $type = $uploadedFile->getMimeType();


            if (strpos($type, 'image') !== false) {
                Storage::disk('local')->putFileAs(
                    '1/media/images/',
                    $uploadedFile,
                    $filename
                );
                $types = 'Image';
            }
            else if (strpos($type, 'video') !== false) {
                Storage::disk('local')->putFileAs(
                    '1/media/video/',
                    $uploadedFile,
                    $filename
                );
                $types = 'Video';
            }
            else if (strpos($type, 'audio') !== false) {
                Storage::disk('local')->putFileAs(
                    '1/media/audio/',
                    $uploadedFile,
                    $filename
                );
                $types = 'Audio';
            }
            else{
                return response()->json(['success' => false, 'code' => 422,'message' => 'File must me image , video or audio.', 'error' => true], 422);
            }


            $media = new Media;
            $media->media = $filename;
            $media->preview = $filename;
            $media->name = 'Admin user';
            $media->type = $types;
            $media->tag = $types;
            $media->upload_by = 'admin@admin.com';
            $media->upload_time = Carbon::now();
            $media->created_at = Carbon::now();
            $media->updated_at = Carbon::now();
            $media->save();

            return response()->json(['success' => true, 'code' => 202,'message' => 'Media file uploaded successfully.', 'error' => false], 202);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function media()
    {
        $medialist = [];
        $images = Storage::disk('local')->files('1/media/images/');
        foreach($images as $image){
            array_push($medialist, url('storage/app')."/".$image);
        }

        $audio = Storage::disk('local')->files('1/media/audio/');
        foreach($audio as $aud){
            array_push($medialist,url('storage/app')."/".$aud);
        }
        $video = Storage::disk('local')->files('1/media/video/');
        foreach($video as $vid){
            array_push($medialist,url('storage/app')."/".$vid);
        }
        
        return response()->json(['success' => true, 'code' => 200 , 'data' => $medialist, 'message' => 'Media list retrieve successfully.']);
    }
}
