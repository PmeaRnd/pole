<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log as LogActivity;
use Auth;
use App\User;
use Hash;
use Log;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        Log::info($request->all());
        if(!$request->has('email') || $request->email == ''){
            return response()->json(['success' => false,'data' => null,'message' => 'Email field required'],400);
        }
        else if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return response()->json(['success' => false,'data' => null,'message' => 'Email is not valid.'],400);
        }

        if(!$request->has('password') || $request->password == ''){
            return response()->json(['success' => false,'data' => null,'message' => 'Password field required'],400);
        }
        else if(!(strlen($request->password) >= 6) ){
            return response()->json(['success' => false,'data' => null,'message' => 'Password must me greater than or equal to 6 char long.'],400);
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $user['token'] =  $user->createToken('MyApp')-> accessToken;
            $log = [];
            $log['subject'] = auth()->user()->name.' login in successfully.';
            $log['url'] = $request->fullUrl();
            $log['method'] = $request->method();
            $log['ip'] = $request->ip();
            $log['agent'] = $request->header('user-agent');
            $log['user_name'] = auth()->check() ? auth()->user()->name : 'Admin user';
            LogActivity::create($log);

            return response()->json(['success' => true,'data' => $user,'message' => 'User login successfully'],200);
        }
        else{
            return response()->json(['success' => false,'data' => null,'message' => 'User unauthorized'],401);
        }

        // if($request->has('mobile_no') && $request->mobile_no != ''){
        //     if(preg_match('/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/', $request->mobile_no)){

        //     }
        //     else{
        //         return response()->json(['success' => true,'data' => $user,'message' => 'Mobile number not valid.'],200);
        //     }
        // }
        // else{
        //     return response()->json(['success' => true,'data' => $user,'message' => 'Please enter mobile number.'],200);
        // }

        // if($request->has('password')){

        // }

        // if($request->has('mobile_no') && $request->has('password'))
        // {
        //     $user = User::where('mobile', $request->mobile_no)->where('password',bcrypt($request->password))->first();
        //     dd($user);
        // }
        // // Get user record
        // $user = User::where('mobile_no', $request->get('mobile_no'))->first();

        // // Check Condition Mobile No. Found or Not
        // if($request->get('mobile_no') != $user->mobile_no) {
        //     \Session::put('errors', 'Your mobile number not match in our system..!!');
        //     return back();
        // }

        // // Set Auth Details
        // \Auth::login($user);

        // if(Auth::attempt(['mobile' => request('mobile_no'), 'password' => request('password')])){
        //   $user = Auth::user();
        //   dd($user);
        // }

        $user = User::where('mobile','=',$request->mobile_no)->where('password','=',Hash::make($request->password))->first();
        dd($user);
    }
}
