<?php

namespace App\Http\Controllers\Api\Display;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Monitor extends Controller
{
    public function index(Request $request)
    {
        $info = ['name' => 'Demo Display', 'model_no' => 'GFS886-12','resolution' => '1024 x 768'];
        return  response()->json(['success' => true, 'code' => 200, 'info' => $info], 200);
    }
}
