<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class cameraStream extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'camera:stream';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call the shell script to stream camera.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filesInFolder = \File::files(storage_path('app/stream'));
        foreach($filesInFolder as $path) {
            $file = pathinfo($path);
            echo $file['filename'] ;
        }
    }
}
