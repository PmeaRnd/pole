<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoleAccess extends Model
{
    protected $table = 'pole_accesses';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pole()
    {
        return $this->belongsTo('App\Models\Pole', 'pole_id', 'identity');
    }
}
