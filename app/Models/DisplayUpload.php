<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DisplayUpload extends Model
{
    protected $table = 'display_uploads';
}
