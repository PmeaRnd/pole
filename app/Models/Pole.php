<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pole extends Model
{
    protected $table = 'poles';

    public function camera()
    {
        return $this->belongsTo('App\Models\Camera','id', 'pole_id');
    }

    public function sensor()
    {
        return $this->belongsTo('App\Models\Sensor', 'id', 'pole_id');
    }

    public function screen()
    {
        return $this->belongsTo('App\Models\Screen', 'id', 'pole_id');
    }
}
