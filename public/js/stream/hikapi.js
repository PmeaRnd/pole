var g_bPTZAuto = false;
var g_iWndIndex = 0;
var g_hikLocalConfig;

// 检查插件是否已经安装过
if (-1 == WebVideoCtrl.I_CheckPluginInstall()) {
	//$.messager.alert('提示', "您还未安装过插件!");
}

// 初始化插件参数及插入插件
WebVideoCtrl.I_InitPlugin("100%", "100%", {
	iWndowType: 2,
	bDebugMode: true,
	cbSelWnd: function(xmlDoc) {
		g_iWndIndex = $(xmlDoc).find("SelectWnd").eq(0).text();
	},
	cbEvent: function(eventType, wndIndex, test) {
		alert(eventType);
	}
});

dateFormat = function(oDate, fmt) {
	var o = {
		"M+": oDate.getMonth() + 1, //月份
		"d+": oDate.getDate(), //日
		"h+": oDate.getHours(), //小时
		"m+": oDate.getMinutes(), //分
		"s+": oDate.getSeconds(), //秒
		"q+": Math.floor((oDate.getMonth() + 3) / 3), //季度
		"S": oDate.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (oDate.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
};

function showOPInfo(szInfo) {
	szInfo = dateFormat(new Date(), "yyyy-MM-dd hh:mm:ss") + " " + szInfo;
	console.log(szInfo);
}

function mouseDownPTZControl(iPTZIndex) {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		iPTZSpeed = 4,
		bStop = false;

	if (oWndInfo != null) {
		if (9 == iPTZIndex && g_bPTZAuto) {
			iPTZSpeed = 0; // 自动开启后，速度置为0可以关闭自动
			bStop = true;
		} else {
			g_bPTZAuto = false; // 点击其他方向，自动肯定会被关闭
			bStop = false;
		}

		WebVideoCtrl.I_PTZControl(iPTZIndex, bStop, {
			iPTZSpeed: iPTZSpeed,
			success: function(xmlDoc) {
				if (9 == iPTZIndex) {
					g_bPTZAuto = !g_bPTZAuto;
				}
			},
			error: function() {}
		});
	}
}

// 方向PTZ停止
function mouseUpPTZControl() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(1, true, {
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZZoomIn() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(10, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZZoomout() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(11, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZZoomStop() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(11, true, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZFocusIn() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(12, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZFoucusOut() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(13, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZFoucusStop() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(12, true, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZIrisIn() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(14, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZIrisOut() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(15, false, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

function PTZIrisStop() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex);

	if (oWndInfo != null) {
		WebVideoCtrl.I_PTZControl(14, true, {
			iWndIndex: g_iWndIndex,
			success: function(xmlDoc) {},
			error: function() {}
		});
	}
}

// 启用电子放大
function clickEnableEZoom() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "";

	if (oWndInfo != null) {
		var iRet = WebVideoCtrl.I_EnableEZoom();
		if (0 == iRet) {
			szInfo = "启用电子放大成功！";
		} else {
			szInfo = "启用电子放大失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}
}

// 禁用电子放大
function clickDisableEZoom() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "";

	if (oWndInfo != null) {
		var iRet = WebVideoCtrl.I_DisableEZoom();
		if (0 == iRet) {
			szInfo = "禁用电子放大成功！";
		} else {
			szInfo = "禁用电子放大失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}
}

// 启用3D放大
function clickEnable3DZoom() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "";

	if (oWndInfo != null) {
		var iRet = WebVideoCtrl.I_Enable3DZoom();
		if (0 == iRet) {
			szInfo = "启用3D放大成功！";
		} else {
			szInfo = "启用3D放大失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}
}

// 禁用3D放大
function clickDisable3DZoom() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "";

	if (oWndInfo != null) {
		var iRet = WebVideoCtrl.I_Disable3DZoom();
		if (0 == iRet) {
			szInfo = "禁用3D放大成功！";
		} else {
			szInfo = "禁用3D放大失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}
}

// 设置预置点
function clickSetPreset(presetID) {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		iPresetID = presetID;

	if (oWndInfo != null) {
		WebVideoCtrl.I_SetPreset(iPresetID, {
			success: function (xmlDoc) {
				showOPInfo(oWndInfo.szIP + " 设置预置点成功！");
			},
			error: function () {
				showOPInfo(oWndInfo.szIP + " 设置预置点失败！");
			}
		});
	}
}

// 调用预置点
function clickGoPreset(presetID) {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		iPresetID = presetID;

	if (oWndInfo != null) {
		WebVideoCtrl.I_GoPreset(iPresetID, {
			success: function (xmlDoc) {
				showOPInfo(oWndInfo.szIP + " 调用预置点成功！");
			},
			error: function () {
				showOPInfo(oWndInfo.szIP + " 调用预置点失败！");
			}
		});
	}
}

// 登录
function clickLogin(szIP, szPort, szUserName, szPassword, iChannelID, autoPlay) {
	if ("" == szIP || "" == szPort) {
		return;
	}

	var iRet = WebVideoCtrl.I_Login(szIP, 1, szPort, szUserName, szPassword, {
		success: function(xmlDoc) {
			showOPInfo(szIP + " 登录成功！");
			if (autoPlay) {
				setTimeout(function() {
					clickStartRealPlay(szIP);
				}, 1000);
			}
		},
		error: function() {
			showOPInfo(szIP + " 登录失败！");
		}
	});

	if (-1 == iRet) {
		showOPInfo(szIP + " 已登录过！");
		if (autoPlay) {
			clickStartRealPlay(szIP, iChannelID);
		}
	}
}

// 退出
function clickLogout(szIP) {
	var iRet = WebVideoCtrl.I_Logout(szIP);
	if (0 == iRet) {} else {}
}

function clickStartRealPlay(szIP, iChannelID) {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		iStreamType = 1,
		bZeroChannel = false,
		szInfo = "";

	if ("" == szIP) {
		return;
	}

	if (oWndInfo != null) { // 已经在播放了，先停止
		WebVideoCtrl.I_Stop();
	}

	var iRet = WebVideoCtrl.I_StartRealPlay(szIP, {
		iStreamType: iStreamType,
		iChannelID: iChannelID,
		bZeroChannel: bZeroChannel
	});

	if (0 == iRet) {
		clickDisableEZoom();
		szInfo = "开始预览成功！";		
	} else {
		szInfo = "开始预览失败！";
	}

	showOPInfo(szIP + " " + szInfo);
}

function clickStopRealPlay() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "";

	if (oWndInfo != null) {
		var iRet = WebVideoCtrl.I_Stop();
		if (0 == iRet) {
			szInfo = "停止预览成功！";
		} else {
			szInfo = "停止预览失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}
}

// 抓图
function clickCapturePic() {
	var oWndInfo = WebVideoCtrl.I_GetWindowStatus(g_iWndIndex),
		szInfo = "",
		szPicName = null;

	if (oWndInfo != null) {
		var szChannelID = 1,
			szPicName = oWndInfo.szIP + "_" + szChannelID + "_" + dateFormat(new Date(), "yyyyMMddhhmmssS"),
			iRet = WebVideoCtrl.I_CapturePic(szPicName);
		if (0 == iRet) {
			szInfo = "抓图成功！";
		} else {
			szInfo = "抓图失败！";
		}
		showOPInfo(oWndInfo.szIP + " " + szInfo);
	}

	return szPicName;
}

// 打开选择框 0：文件夹  1：文件
function clickOpenFileDlg(id, iType) {
	return WebVideoCtrl.I_OpenFileDlg(iType);
}

// 获取本地参数
function clickGetLocalCfg() {
	if (g_hikLocalConfig == null || typeof(g_hikLocalConfig) == 'undefined') {
		g_hikLocalConfig = {};
	}
	var xmlDoc = WebVideoCtrl.I_GetLocalCfg();

	g_hikLocalConfig.netsPreach = $(xmlDoc).find("BuffNumberType").eq(0).text();
	g_hikLocalConfig.wndSize = $(xmlDoc).find("PlayWndType").eq(0).text();
	g_hikLocalConfig.rulesInfo = $(xmlDoc).find("IVSMode").eq(0).text();
	g_hikLocalConfig.captureFileFormat = $(xmlDoc).find("CaptureFileFormat").eq(0).text();
	g_hikLocalConfig.packSize = $(xmlDoc).find("PackgeSize").eq(0).text();
	g_hikLocalConfig.recordPath = $(xmlDoc).find("RecordPath").eq(0).text();
	g_hikLocalConfig.downloadPath = $(xmlDoc).find("DownloadPath").eq(0).text();
	g_hikLocalConfig.previewPicPath = $(xmlDoc).find("CapturePath").eq(0).text();
	g_hikLocalConfig.playbackPicPath = $(xmlDoc).find("PlaybackPicPath").eq(0).text();
	g_hikLocalConfig.playbackFilePath = $(xmlDoc).find("PlaybackFilePath").eq(0).text();
	g_hikLocalConfig.protocolType = $(xmlDoc).find("ProtocolType").eq(0).text();

	showOPInfo("本地配置获取成功！");

	return g_hikLocalConfig;
}

// 设置本地参数
function clickSetLocalCfg(localConfig) {
	var arrXml = [],
		szInfo = "";

	arrXml.push("<LocalConfigInfo>");
	arrXml.push("<PackgeSize>" + localConfig.packSize + "</PackgeSize>");
	arrXml.push("<PlayWndType>" + localConfig.wndSize + "</PlayWndType>");
	arrXml.push("<BuffNumberType>" + localConfig.netsPreach + "</BuffNumberType>");
	arrXml.push("<RecordPath>" + localConfig.recordPath + "</RecordPath>");
	arrXml.push("<CapturePath>" + localConfig.previewPicPath + "</CapturePath>");
	arrXml.push("<PlaybackFilePath>" + localConfig.playbackPicPath + "</PlaybackFilePath>");
	arrXml.push("<PlaybackPicPath>" + localConfig.playbackPicPath + "</PlaybackPicPath>");
	arrXml.push("<DownloadPath>" + localConfig.downloadPath + "</DownloadPath>");
	arrXml.push("<IVSMode>" + localConfig.rulesInfo + "</IVSMode>");
	arrXml.push("<CaptureFileFormat>" + localConfig.captureFileFormat + "</CaptureFileFormat>");
	arrXml.push("<ProtocolType>" + localConfig.protocolType + "</ProtocolType>");
	arrXml.push("</LocalConfigInfo>");

	var iRet = WebVideoCtrl.I_SetLocalCfg(arrXml.join(""));

	if (0 == iRet) {
		szInfo = "本地配置设置成功！";
	} else {
		szInfo = "本地配置设置失败！";
	}
	showOPInfo(szInfo);
}