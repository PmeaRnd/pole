<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisplayUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_uploads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lampost_id');
            $table->integer('user_id')->nullable();
            $table->string('media_type');
            $table->string('filename')->nullable();
            $table->string('media_text')->nullable();
            $table->json('media_prop')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('display_uploads');
    }
}
