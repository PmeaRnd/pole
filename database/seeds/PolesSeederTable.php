<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
use Carbon\Carbon;

class PolesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $id = 1;
    	foreach (range(1,25) as $index) {
            $id++;
            $str=rand();
            $result = md5($str);
	        DB::table('poles')->insert([
	            'name' => "Pole_$id",
	            'identity' => $result,
                'ip' => $faker->ipv4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
	        ]);
        }
    }
}
