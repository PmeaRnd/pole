<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Faker\Factory as Faker;

class MediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('media')->insert([
	            'name' => $faker->name,
	            'preview' => 'preview.jpg',
                'type' => 'video',
                'tag' => 'video',
                'upload_by' => 'admin@admin.com',
                'upload_time' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
	        ]);
	    }
    }
}
