<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Faker\Factory as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();
    	// foreach (range(1,100) as $index) {
	    //     DB::table('users')->insert([
	    //         'name' => $faker->name,
	    //         'email' => $faker->email,
	    //         'password' => bcrypt('secret'),
	    //     ]);
        // }



	        DB::table('users')->insert([
                'name' => 'Virendra Arekar',
                'mobile' => 8483988837,
	            'email' => 'virendra.arekar@gmail.com',
                'password' => bcrypt('viren45mca'),
                'is_admin' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
	        ]);
    }
}
