<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProgramsTableSeeeder::class);
        $this->call(InfosTableSeeder::class);
        $this->call(MediasTableSeeder::class);
        $this->call(PolesSeederTable::class);
    }
}
