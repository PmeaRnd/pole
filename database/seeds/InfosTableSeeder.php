<?php

use Illuminate\Database\Seeder;
use App\Models\Info;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Faker\Factory as Faker;

class InfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('infos')->insert([
	            'name' => $faker->name,
	            'company' => "company-".$faker->name,
                'time' => Carbon::now(),
                'status' => 'online',
                'handler' => "handler-".$faker->name,
                'hander_time' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
	        ]);
	    }
    }
}
