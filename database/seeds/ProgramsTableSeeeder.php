<?php

use Illuminate\Database\Seeder;
use App\Models\Program;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Faker\Factory as Faker;

class ProgramsTableSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('programs')->insert([
	            'name' => $faker->name,
	            'height' => 160,
                'weight' => 240,
                'tag' => 'unclassified',
                'creator' => $faker->name,
                'renew_time' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
	        ]);
	    }
    }
}
