<?php
Auth::routes(['register' => false]);
Route::middleware(['auth','admin'])->group(function () {
    // Home

    // Route::get('/admin/home', 'HomeController@index')->name('home');
    // Route::get('/home', 'HomeController@index');
    // User CRUD
    Route::resource('/admin/user','Admin\UserController');

    // Super User CRUD
    Route::resource('/admin/superuser','Admin\SuperUser');

    // Map
    Route::resource('map','Admin\GoogleMap');

    // Dashboard
    Route::get('admin','Admin\Dashboard@index')->name('home');;

    // Lamppost Routing
    Route::get('admin/lamppost','Admin\Lamppost@home');
    Route::get('admin/lamppost/map','Admin\Lamppost@map');
    Route::get('admin/lamppost/cameras','Admin\Lamppost@cameras');
    Route::get('admin/lamppost/aqi','Admin\Lamppost@aqi');
    Route::get('admin/lamppost/lights','Admin\Lamppost@lights');

    // Programs CRUD
    Route::resource('admin/lamppost/programs', 'Admin\Program');
    Route::get('admin/lamppost/programs/send/{id}', 'Admin\Program@send');
    Route::get('admin/lamppost/programs/info/{id}', 'Admin\Program@info');
    Route::get('admin/lamppost/programs/query/{id}', 'Admin\Program@query');
    Route::post('admin/lamppost/programs/make', 'Admin\Program@make');

    // Emergency Routing
    Route::resource('admin/lamppost/emergency', 'Admin\Emergency');
    Route::get('admin/lamppost/emergency_call', 'Admin\Emergency@call');
    Route::get('admin/lamppost/emergency_info', 'Admin\Emergency@info');

    // Group
    Route::resource('admin/lamppost/group', 'Admin\group');
    Route::resource('admin/lamppost/medias', 'Admin\Media');
    Route::post('admin/lamppost/media', 'Admin\Media@media');
    Route::resource('admin/lamppost/settings', 'Admin\Setting');

    // Display
    Route::get('admin/lamppost/displays','Admin\Display@index');
    Route::post('admin/lamppost/displays/buffer','Admin\Display@buffer')->name('buffer');
    Route::post('admin/lamppost/displays/upload','Admin\Display@upload')->name('display.upload');

    // Log Activity
    Route::resource('admin/lamppost/logs','Admin\ActivityLog');

    // Video stream test
    Route::get('video/streaming','Log\DebugLog@index');

    // configure the  inividual device setup
    Route::get('admin/setup','Admin\Setup@index');

    // Crud for smart pole list
    Route::resource('admin/pole','Admin\SmartPole');

    // camera list for rightsidebar
    Route::post('lamppost/cameralist','Admin\Camera@camera_list')->name('camera.list');
    Route::post('lamppost/camerastream','Admin\Camera@stream')->name('camera.stream');


});



Route::middleware(['auth','client'])->group(function () {
    Route::get('/', 'Client\Dashboard@index');

    // Map
    Route::resource('map','Admin\GoogleMap');

    // Lamppost Routing
    Route::get('lamppost','Client\Lamppost@home');
    Route::get('lamppost/map','Client\Lamppost@map');
    Route::get('lamppost/cameras','Client\Lamppost@cameras');
    Route::get('lamppost/aqi','Client\Lamppost@aqi');
    Route::get('lamppost/lights','Client\Lamppost@lights');

    // Programs CRUD
    Route::resource('lamppost/program', 'Client\Program');
    Route::get('lamppost/programs/send/{id}', 'Client\Program@send');
    Route::get('lamppost/programs/info/{id}', 'Client\Program@info');
    Route::get('lamppost/programs/query/{id}', 'Client\Program@query');
    Route::post('lamppost/programs/make', 'Client\Program@make');

    // Emergency Routing
    Route::resource('lamppost/emergency', 'Client\Emergency');
    Route::get('lamppost/emergency_call', 'Client\Emergency@call');
    Route::get('lamppost/emergency_info', 'Client\Emergency@info');

    // Group
    Route::resource('lamppost/group', 'Client\group');
    Route::resource('lamppost/media', 'Client\Media');
    Route::post('lamppost/media', 'Client\Media@media');
    Route::resource('lamppost/settings', 'Client\Setting');

    // Display
    Route::get('lamppost/displays','Client\Display@index');

    // Log Activity
    Route::resource('lamppost/log','Client\ActivityLog');


});
Route::get('store','Admin\Camera@store')->name('camera.store');

// Route::resource('client','Payment\Client');
Route::get('{slug}','HomeController@slug');
